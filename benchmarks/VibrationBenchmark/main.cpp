#include <chrono>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdlib.h>

#include "../../MPL3115A2/MPL3115A2.h"
#include "../../FlightDriver/FlightDriver.h"
#include "../../ICM20948/ICM20948.h"
#include "../../Util/Helper.h"

int main()
{
    // Set decimal precision of cout
    std::cout.precision(50);

    std::ofstream log_stream;
    log_stream.open("log.csv");
    log_stream << "acce_x,acce_y,acce_z,gyro_x,gyro_y,gyro_z,magn_x,magn_y,magn_z,icm_time\n";

    ////////////////////////////////////////////
    // Init devices
    ////////////////////////////////////////////
    FlightDriver::FlightDriver fd = FlightDriver::FlightDriver(FlightDriver::I2C_ADDRESS);
    ICM20948::ICM20948 icm = ICM20948::ICM20948(ICM20948::I2C_ADDRESS);

    ////////////////////////////////////////////
    // Variables ICM
    ////////////////////////////////////////////
    float gravity = 9.813f;
    unsigned int testsize = 100;
    float acc_derivations_list[testsize][3];
    float gyro_derivations_list[testsize][3];
    double PI = 2 * acos(.0f);

    ////////////////////////////////////////////
    // Setup FlightDriver
    ////////////////////////////////////////////
    int result = fd.begin(true);
    std::cout << "Begin FlightDriver: " << result << std::endl;
    if (result < 0) {
        return -1;
    }

    ////////////////////////////////////////////
    // Setup ICM
    ////////////////////////////////////////////
    result = icm.begin(false);
    std::cout << "Begin ICM: " << result << std::endl;
    if (result < 0) {
        return -1;
    }

    for (int i = 0; i < testsize; ++i) {
        sensors_event_t accel;
        sensors_event_t gyro;
        sensors_event_t mag;
        icm.getEvent(0, &accel, &gyro, &mag);
        acc_derivations_list[i][0] = accel.acceleration.x;
        acc_derivations_list[i][1] = accel.acceleration.y;
        acc_derivations_list[i][2] = accel.acceleration.z;
        gyro_derivations_list[i][0] = gyro.gyro.x;
        gyro_derivations_list[i][1] = gyro.gyro.y;
        gyro_derivations_list[i][2] = gyro.gyro.z;
    }
    float sumx = .0f, sumy = .0f, sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += acc_derivations_list[i][0];
        sumy += acc_derivations_list[i][1];
        sumz += acc_derivations_list[i][2];
    }
    float acc_derivation[3] = {
        sumx / testsize,
        sumy / testsize,
        sumz / testsize - gravity
    };

    sumx = sumy = sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += gyro_derivations_list[i][0];
        sumy += gyro_derivations_list[i][1];
        sumz += gyro_derivations_list[i][2];
    }
    float gyro_derivation[3] = {
        sumx / testsize,
        sumy / testsize,
        sumz / testsize,
    };

    std::cout << "The acceleration derivation is: (" << acc_derivation[0] << ", " << acc_derivation[1] << ", " << acc_derivation[2] << ")" << std::endl;
    std::cout << "The gyro derivation is: (" << gyro_derivation[0] << ", " << gyro_derivation[1] << ", " << gyro_derivation[2] << ")" << std::endl;

    //     ////////////////////////////////////////////
    //     // Test FlightDriver
    //     ////////////////////////////////////////////
    //     unsigned int fd_runs = 5;
    //     for (size_t i = 0; i < fd_runs; i++)
    //     {
    //         int reg = 0;
    //         int val = 0;
    //         std::cout << "Enter reg: ";
    //         std::cin >> reg;
    //         std::cout << "Enter val: ";
    //         std::cin >> val;
    //
    //         std::cout << i + 1 << "/" << fd_runs << " - Send Data: " << fd.setDuty(reg, val) << std::endl;
    //     }

    sensors_event_t accel;
    sensors_event_t gyro;
    sensors_event_t mag;
    std::chrono::steady_clock::time_point icm_begin;
    bool icm_successful;
    std::chrono::steady_clock::time_point icm_end;

    double corrected_accel[3];
    double corrected_gyro[3];
    double corrected_mag[3];

    uint8_t duty = 5;

    fd.setDuty(0, duty);
    fd.setDuty(1, duty);
    fd.setDuty(2, duty);
    fd.setDuty(3, duty);

    sleep(3000);

    for (size_t i = 0; i < 10000; i++) {

        icm_begin = std::chrono::steady_clock::now();
        icm_successful = icm.getEvent(0, &accel, &gyro, &mag);
        icm_end = std::chrono::steady_clock::now();

        corrected_accel[0] = accel.acceleration.x - acc_derivation[0];
        corrected_accel[1] = accel.acceleration.y - acc_derivation[1];
        corrected_accel[2] = accel.acceleration.z - acc_derivation[2];
        corrected_gyro[0] = gyro.gyro.x - gyro_derivation[0];
        corrected_gyro[1] = gyro.gyro.y - gyro_derivation[1];
        corrected_gyro[2] = gyro.gyro.z - gyro_derivation[2];
        corrected_mag[0] = (mag.magnetic.x + ((106.8 / 2) - 30.15)) * 106.8 / 98;
        corrected_mag[1] = (mag.magnetic.y + ((106.5 / 2) - 37.65)) * 106.5 / 98;
        corrected_mag[2] = (mag.magnetic.z + ((103.2 / 2) - 50.85)) * 103.2 / 98;

        if (icm_successful) {
            log_stream << corrected_accel[0] << "," << corrected_accel[1] << "," << corrected_accel[2] << ",";
            log_stream << corrected_gyro[0] << "," << corrected_gyro[1] << "," << corrected_gyro[2] << ",";
            log_stream << corrected_mag[0] << "," << corrected_mag[1] << "," << corrected_mag[2] << ",";
        } else {
            log_stream << "err,err,err,";
            log_stream << "err,err,err,";
            log_stream << "err,err,err,";
        }
        log_stream << std::chrono::duration_cast<std::chrono::microseconds>(icm_end - icm_begin).count();

        log_stream << "\n";
    }

    fd.setDuty(0, 0);
    fd.setDuty(1, 0);
    fd.setDuty(2, 0);
    fd.setDuty(3, 0);

    ////////////////////////////////////////////
    // Ends
    ////////////////////////////////////////////
    std::cout << "End FlightDriver: " << fd.end(false) << std::endl;
    std::cout << "End ICM: " << icm.end(true) << std::endl;

    log_stream.close();

    return 0;
}
