#include <iostream>

#include <math.h>

#include "../src/MPL3115A2/MPL3115A2.h"
#include "../src/Util/Helper.h"

int main () {
    // Set decimal precision of cout
    std::cout.precision(50);

    // Init device
    MPL3115A2::MPL3115A2 baro = MPL3115A2::MPL3115A2(MPL3115A2::I2C_ADDRESS);

    float pressure;
    float altitude;
    float temp;

    // Setup Baro
    int result = baro.begin();
    std::cout << "Begin: " << result << std::endl;
    if (result < 0) { return -1; }
    baro.setSeaPressure(101326);
    baro.setAltitudeOffset(0);
    baro.setPressureOffset(0);
    baro.setTemperatureOffset(0);

    for (size_t i = 0; i < 5; i++)
    {
        // Get pressure data
        int error_pressure = baro.getPressure(&pressure);
        if (error_pressure == 0)
        {
            std::cout << "Pressure: " << pressure << std::endl;
            float p0 = 101326.0f;
            float off = 0.0f;
            float calculated_altitude = 44330.77 * (1 - pow((pressure / p0), 0.1902632f)) + off;
            std::cout << "Calculated Altitude: " << calculated_altitude << std::endl;
        } 
        else 
            std::cout << "Pressure Error: " << error_pressure << std::endl;
        

        // Get altitude data
        int error_altitude = baro.getAltitude(&altitude);
        if (error_altitude == 0)
            std::cout << "Altitude: " << altitude << std::endl;
        else
            std::cout << "Altitude Error: " << error_altitude << std::endl;

        // Get temperature data
        int error_temp = baro.getTemperature(&temp);
        if (error_temp == 0)
            std::cout << "Temperature: " << temp << std::endl;
        else
            std::cout << "Temperature Error: " << error_temp << std::endl;

        std::cout << "************************************" << std::endl;
 
        Util::sleep(2000);
    }

    std::cout << "End: " << baro.end() << std::endl;
    
    return 0;
}
