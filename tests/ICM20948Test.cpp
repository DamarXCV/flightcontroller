#include <iostream>

#include <math.h>
#include "../src/ICM20948/ICM20948.h"
#include "../src/Util/Helper.h"

int main () {

    ICM20948::ICM20948 icm = ICM20948::ICM20948(ICM20948::I2C_ADDRESS);

    int result = icm.begin();
    std::cout << "Begin: " << result << std::endl;
    if (result < 0) { return -1; }

    // icm.setAccelRange(ACCEL_RANGE_16_G);
    std::cout << "Accelerometer range set to: ";
    switch (icm.getAccelRange()) {
        case ICM20948::ACCEL_RANGE_2_G:
            std::cout << "+-2G" << std::endl;
            break;
        case ICM20948::ACCEL_RANGE_4_G:
            std::cout << "+-4G" << std::endl;
            break;
        case ICM20948::ACCEL_RANGE_8_G:
            std::cout << "+-8G" << std::endl;
            break;
        case ICM20948::ACCEL_RANGE_16_G:
            std::cout << "+-16G" << std::endl;
            break;
    }
    // icm.setGyroRange(GYRO_RANGE_2000_DPS);
    std::cout << "Gyro range set to: ";
    switch (icm.getGyroRange()) {
        case ICM20948::GYRO_RANGE_250_DPS:
            std::cout << "250 degrees/s" << std::endl;
            break;
        case ICM20948::GYRO_RANGE_500_DPS:
            std::cout << "500 degrees/s" << std::endl;
            break;
        case ICM20948::GYRO_RANGE_1000_DPS:
            std::cout << "1000 degrees/s" << std::endl;
            break;
        case ICM20948::GYRO_RANGE_2000_DPS:
            std::cout << "2000 degrees/s" << std::endl;
            break;
    }

    //  icm.setAccelRateDivisor(4095);
    unsigned int accel_divisor = icm.getAccelRateDivisor();
    float accel_rate = 1125 / (1.0 + accel_divisor);

    std::cout << "Accelerometer data rate divisor set to: " << accel_divisor << std::endl;
    std::cout << "Accelerometer data rate is approximately: " << accel_rate << " Hz" << std::endl;

    //  icm.setGyroRateDivisor(255);
    unsigned int gyro_divisor = icm.getGyroRateDivisor();
    float gyro_rate = 1100 / (1.0 + gyro_divisor);

    std::cout << "Gyro data rate divisor set to: " << gyro_divisor << std::endl;
    std::cout << "Gyro data rate is approximately: " << gyro_rate << " Hz" << std::endl;

    // icm.setMagDataRate(AK09916_MAG_DATARATE_100_HZ);
    std::cout << "Magnetometer data rate set to: ";
    switch (icm.getMagDataRate()) {
        case ICM20948::AK09916_MAG_DATARATE_SHUTDOWN:
            std::cout << "Shutdown" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_SINGLE:
            std::cout << "Single/One shot" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_10_HZ:
            std::cout << "10 Hz" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_20_HZ:
            std::cout << "20 Hz" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_50_HZ:
            std::cout << "50 Hz" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_100_HZ:
            std::cout << "100 Hz" << std::endl;
            break;
    }

    float gravity = 9.813;
    unsigned int testsize = 100;
    //float last_acc [testsize];
    float acc_derivations_list [testsize][3];
    float gyro_derivations_list [testsize][3];
    //float mag_derivations_list [testsize][3];
    for (int i = 0; i < testsize; ++i) {
        sensors_event_t accel;
        sensors_event_t gyro;
        sensors_event_t mag;
        icm.getEvent(0, &accel, &gyro, &mag);
        acc_derivations_list[i][0] = accel.acceleration.x;
        acc_derivations_list[i][1] = accel.acceleration.y;
        acc_derivations_list[i][2] = accel.acceleration.z;
        gyro_derivations_list[i][0] = gyro.gyro.x;
        gyro_derivations_list[i][1] = gyro.gyro.y;
        gyro_derivations_list[i][2] = gyro.gyro.z;
        /*mag_derivations_list[i][0] = mag.magnetic.x;
        mag_derivations_list[i][1] = mag.magnetic.y;
        mag_derivations_list[i][2] = mag.magnetic.z;*/
    }
    float sumx = .0f, sumy = .0f, sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += acc_derivations_list[i][0];
        sumy += acc_derivations_list[i][1];
        sumz += acc_derivations_list[i][2];
    }
    float acc_derivation[3] = {
        sumx / testsize,
        sumy / testsize,
        sumz / testsize - gravity
    };

    sumx = sumy = sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += gyro_derivations_list[i][0];
        sumy += gyro_derivations_list[i][1];
        sumz += gyro_derivations_list[i][2];
    }
    float gyro_derivation[3] = {
        sumx / testsize,
        sumy / testsize,
        sumz / testsize,
    };

    /*sumx = sumy = sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += mag_derivations_list[i][0];
        sumy += mag_derivations_list[i][1];
        sumz += mag_derivations_list[i][2];
    }
    float mag_derivation[3] = {
        sumx / testsize,
        sumy / testsize,
        sumz / testsize,
    };*/

    std::cout << "The acceleration derivation is: (" << acc_derivation[0] << ", " << acc_derivation[1] << ", " << acc_derivation[2] << ")" << std::endl;
    std::cout << "The gyro derivation is: (" << gyro_derivation[0] << ", " << gyro_derivation[1] << ", " << gyro_derivation[2] << ")" << std::endl;
    //std::cout << "The magnetometer derivation is: (" << mag_derivation[0] << ", " << mag_derivation[1] << ", " << mag_derivation[2] << ")" << std::endl;

    double PI = 2*acos(.0f);

    for (size_t i = 0; i < 5; i++)
    {
        sensors_event_t accel;
        sensors_event_t gyro;
        sensors_event_t mag;
        sensors_event_t temp;
        icm.getEvent(0, &accel, &gyro, &mag);

        double corrected_accel[3] = {accel.acceleration.x - acc_derivation[0], accel.acceleration.y - acc_derivation[1], accel.acceleration.z - acc_derivation[2]};
        double corrected_gyro[3] = {gyro.gyro.x - gyro_derivation[0], gyro.gyro.y - gyro_derivation[1], gyro.gyro.z - gyro_derivation[2]};
        double corrected_mag[3] = {(mag.magnetic.x+((106.8/2)-30.15))*106.8/98, (mag.magnetic.y+((106.5/2)-37.65))*106.5/98, (mag.magnetic.z+((103.2/2)-50.85))*103.2/98};

        float pitch_rad = -atan2(corrected_accel[0], sqrt(pow(corrected_accel[1], 2) + pow(corrected_accel[2], 2)));
        float roll_rad = atan2(corrected_accel[1], corrected_accel[2]);
        float pitch = pitch_rad * (180.0 / PI);
        float roll = roll_rad * (180.0 / PI);
        float xh = corrected_mag[0]*cos(pitch_rad)+corrected_mag[1]*sin(roll_rad)*sin(pitch_rad)-corrected_mag[2]*cos(roll_rad)*sin(pitch_rad);
        float yh = corrected_mag[1]*cos(roll_rad)+corrected_mag[2]*sin(roll_rad);
        float heading_rad = atan2(yh, xh);
        float heading = heading_rad * (180.0 / PI);

        std::cout << "*******************" << std::endl;
        // std::cout << "\t\tTemperature: " << temp.temperature << " deg C" << std::endl;
        std::cout << "Accel X: " << corrected_accel[0] << " \tY: " << corrected_accel[1] << " \tZ: " << corrected_accel[2] << " m/s^2 " << std::endl;
        std::cout << "Gyro X: " << corrected_gyro[0] << " \tY: " << corrected_gyro[1] << " \tZ: " << corrected_gyro[2] << " radians/s " << std::endl;
        std::cout << "Mag X: " << mag.magnetic.x << " \tY: " << mag.magnetic.y << " \tZ: " << mag.magnetic.z << " uT" << std::endl;
        std::cout << "Pitch: " << pitch << "°, Roll: " << roll << "°" << std::endl;
        std::cout << "Pitch: " << pitch_rad << "rad, Roll: " << roll_rad << "rad" << std::endl;
        std::cout << "yh: " << yh << ", xh: " << xh << std::endl;
        std::cout << "Heading rad: " << heading_rad << std::endl;
        std::cout << "Heading: " << heading << std::endl;
        Util::sleep(250);

    }

    std::cout << "End: " << icm.end() << std::endl;

    return 0;
}