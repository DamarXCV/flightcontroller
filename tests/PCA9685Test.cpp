#include <iostream>

#include <chrono>
#include <thread>
#include <pigpio.h>

#include "../src/PCA9685/PCA9685.h"
#include "../src/Util/Helper.h"


const int PWM_PIN_0 = 0;
const int PWM_PIN_1 = 1;
const int PWM_PIN_2 = 2;
const int PWM_PIN_3 = 3;

const int CURRENT_PWM_PIN = PWM_PIN_3;

const float baseDuty = 25.0f;
float duty_pin_0 = .0f,
        duty_pin_1 = .0f,
        duty_pin_2 = .0f,
        duty_pin_3 = .0f;

int main () {
    // Set decimal precision of cout
    std::cout.precision(50);

    // Init device
    PCA9685::PCA9685 pwm = PCA9685::PCA9685(PCA9685::I2C_ADDRESS);
    
    // Setup
    int result = pwm.begin();
    std::cout << "Begin: " << result << std::endl;
    if (result < 0) { return -1; }

    // Set PWM Freq to 50Hz
    pwm.setPWMFreq(50);

    // Init PPM
    std::cout << "PPM signal initializing..." << std::endl;
    // set PWM to 0
    pwm.setServoPPM(PWM_PIN_0, 0.0f);
    pwm.setServoPPM(PWM_PIN_1, 0.0f);
    pwm.setServoPPM(PWM_PIN_2, 0.0f);
    pwm.setServoPPM(PWM_PIN_3, 0.0f);
    Util::sleep(1000);

    std::cout << "PPM signal initialized" << std::endl;

    // Run Duty
    for (int i = 0; i <= 100; i++)
    {
        std::cout << "PPM: " << i << std::endl;

        // pwm.setServoPPM(CURRENT_PWM_PIN, i);
        pwm.setServoPPM(PWM_PIN_0, i);
        pwm.setServoPPM(PWM_PIN_1, i);
        pwm.setServoPPM(PWM_PIN_2, i);
        pwm.setServoPPM(PWM_PIN_3, i);
        Util::sleep(40);
    }
    for (int i = 100; i >= 0; i--)
    {
        std::cout << "PPM: " << i << std::endl;

        // pwm.setServoPPM(CURRENT_PWM_PIN, i);
        pwm.setServoPPM(PWM_PIN_0, i);
        pwm.setServoPPM(PWM_PIN_1, i);
        pwm.setServoPPM(PWM_PIN_2, i);
        pwm.setServoPPM(PWM_PIN_3, i);
        Util::sleep(40);
    }

    // Stop PPM
    // set PWM to 0
    pwm.setServoPPM(PWM_PIN_0, -100.0f);
    pwm.setServoPPM(PWM_PIN_1, -100.0f);
    pwm.setServoPPM(PWM_PIN_2, -100.0f);
    pwm.setServoPPM(PWM_PIN_3, -100.0f);

    std::cout << "PPM signal terminated" << std::endl;

    std::cout << "End: " << pwm.end() << std::endl;

    return 0;
}
