#include <iostream>

#include <chrono>
#include <thread>
#include <pigpio.h>

#include "../src/Util/Helper.h"
#include "../src/Telemetry/Telemetry.h"


int main () {
    // Set decimal precision of cout
    std::cout.precision(50);

    Telemetry::Telemetry *t = new Telemetry::Telemetry();
    
    return 0;
}
