#include <iostream>

#include "../src/FlightDriver/FlightDriver.h"

int main () {
    unsigned int runs = 5;

    // Set decimal precision of cout
    std::cout.precision(50);

    FlightDriver::FlightDriver fd = FlightDriver::FlightDriver(FlightDriver::I2C_ADDRESS);

    std::cout << "Init FlightDriver: " << fd.begin() << std::endl;
    for (size_t i = 0; i < runs; i++)
    {
        int reg = 0;
        int val = 0;
        std::cout << "Enter reg: ";
        std::cin >> reg;
        std::cout << "Enter val: ";
        std::cin >> val;

        std::cout << i + 1 << "/" << runs << " - Send Data: " << fd.setDuty(reg, val) << std::endl;
    }
    std::cout << "End FlightDriver: " << fd.end() << std::endl;
}
