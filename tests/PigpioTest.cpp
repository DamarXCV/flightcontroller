#include <iostream>
#include <pigpio.h>

int main()
{
    std::cout << "gpioInitialise: " << gpioInitialise() << std::endl;
    gpioTerminate();
    return 1;
}
