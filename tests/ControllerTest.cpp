#include <iostream>

#include <pigpio.h>
#include <math.h>

#include "../src/Util/Helper.h"
#include "../src/ICM20948/ICM20948.h"
#include "../src/PWM/PCA9685.h"

class Controller {
public:
    void initPPM(PCA9685::PCA9685* pwm);
    void testPPM(PCA9685::PCA9685* pwm);
    void setPPM(PCA9685::PCA9685* pwm);
    void stopPPM(PCA9685::PCA9685* pwm);
    void initDuty(PCA9685::PCA9685* pwm);
    void setDuty(PCA9685::PCA9685* pwm);
    void stopDuty(PCA9685::PCA9685* pwm);
    void initBuffer(ICM20948::ICM20948* icm);
    void initICM(ICM20948::ICM20948* icm);
    void calcDerivation(ICM20948::ICM20948* icm);
    void getAlignment(ICM20948::ICM20948* icm);
    void calcAlignment(void);

private:
    const char PWM_PIN_0 = 0,
               PWM_PIN_1 = 1,
               PWM_PIN_2 = 2,
               PWM_PIN_3 = 3;
        
    const double PI = 2*acos(.0f);
    const float baseDuty = 25.0f;
    const float maxDutyDerivation = 20.0f;  // max baseDuty
    const float gravity = 9.813;
    const unsigned char testsize = 100;

    static const unsigned char buffersize = 3;

    unsigned char bufferpos = 0;

    float duty_pin_0 = .0f,
          duty_pin_1 = .0f,
          duty_pin_2 = .0f,
          duty_pin_3 = .0f;

    float acc_derivation[3],
    //      mag_derivation[3],
          gyro_derivation[3];

    // https://terpconnect.umd.edu/~toh/spectrum/Smoothing.html
    float lastAcc[buffersize][3],
          lastMag[buffersize][3],
          lastGyro[buffersize][3];

    float pitch_rad   = .0f,
          roll_rad    = .0f,
          heading_rad = .0f;
};


void Controller::initPPM(PCA9685::PCA9685* pwm) {

    std::cout << "PPM signal initializing..." << std::endl;
    // set PWM to 0
    pwm->setServoPPM(PWM_PIN_0, .0f);
    pwm->setServoPPM(PWM_PIN_1, .0f);
    pwm->setServoPPM(PWM_PIN_2, .0f);
    pwm->setServoPPM(PWM_PIN_3, .0f);

    Util::sleep(1500);
    std::cout << "PPM signal initialized" << std::endl;
}

void Controller::testPPM(PCA9685::PCA9685* pwm) {
    for (int i = 0; i <= 100; i++)
    {
        std::cout << "PPM: " << i << "%" << std::endl;

        pwm->setServoPPM(PWM_PIN_0, i);
        pwm->setServoPPM(PWM_PIN_1, i);
        pwm->setServoPPM(PWM_PIN_2, i);
        pwm->setServoPPM(PWM_PIN_3, i);
        Util::sleep(50);
    }

    for (int i = 100; i >= 0; i--)
    {
        std::cout << "PPM: " << i << "%" << std::endl;

        pwm->setServoPPM(PWM_PIN_0, i);
        pwm->setServoPPM(PWM_PIN_1, i);
        pwm->setServoPPM(PWM_PIN_2, i);
        pwm->setServoPPM(PWM_PIN_3, i);
        Util::sleep(50);
    }

}

void Controller::stopPPM(PCA9685::PCA9685* pwm) {

    // set PWM to 0
    pwm->setServoPPM(PWM_PIN_0, -100.0f);
    pwm->setServoPPM(PWM_PIN_1, -100.0f);
    pwm->setServoPPM(PWM_PIN_2, -100.0f);
    pwm->setServoPPM(PWM_PIN_3, -100.0f);

    std::cout << "PPM signal terminated" << std::endl;
}

void Controller::setPPM(PCA9685::PCA9685* pwm) {
    pwm->setServoPPM(PWM_PIN_0, duty_pin_0);
    pwm->setServoPPM(PWM_PIN_1, duty_pin_1);
    pwm->setServoPPM(PWM_PIN_2, duty_pin_2);
    pwm->setServoPPM(PWM_PIN_3, duty_pin_3);
}

void Controller::initDuty(PCA9685::PCA9685* pwm) {
    for(float i = 1.0f; i < baseDuty; ++i) {
        Util::sleep(40);
        duty_pin_0 = i;
        duty_pin_1 = i;
        duty_pin_2 = i;
        duty_pin_3 = i;
        setPPM(pwm);
    }
}

void Controller::setDuty(PCA9685::PCA9685* pwm) {
    
    float pitch = pitch_rad * (180.0f / PI);
    float roll = roll_rad * (180.0f / PI);
    
    duty_pin_0 = baseDuty + ((maxDutyDerivation/3.0f) * ((-roll/90.0f)+( pitch/90.0f)));
    duty_pin_1 = baseDuty + ((maxDutyDerivation/3.0f) * ((-roll/90.0f)+(-pitch/90.0f)));
    duty_pin_2 = baseDuty + ((maxDutyDerivation/3.0f) * (( roll/90.0f)+( pitch/90.0f)));
    duty_pin_3 = baseDuty + ((maxDutyDerivation/3.0f) * (( roll/90.0f)+(-pitch/90.0f)));

    std::cout << "M0: " << duty_pin_0 << "%" << std::endl;
    std::cout << "M1: " << duty_pin_1 << "%" << std::endl;
    std::cout << "M2: " << duty_pin_2 << "%" << std::endl;
    std::cout << "M3: " << duty_pin_3 << "%" << std::endl;

    setPPM(pwm);
}

void Controller::stopDuty(PCA9685::PCA9685* pwm) {
    for(float i = baseDuty; i > 0; --i) {
        Util::sleep(40);
        duty_pin_0 = i;
        duty_pin_1 = i;
        duty_pin_2 = i;
        duty_pin_3 = i;
        setPPM(pwm);
    }
}

void Controller::initICM(ICM20948::ICM20948* icm) {

    // icm->setAccelRange(ACCEL_RANGE_16_G);
    std::cout << "Accelerometer range set to: ";
    switch (icm->getAccelRange()) {
        case ICM20948::ACCEL_RANGE_2_G:
            std::cout << "+-2G" << std::endl;
            break;
        case ICM20948::ACCEL_RANGE_4_G:
            std::cout << "+-4G" << std::endl;
            break;
        case ICM20948::ACCEL_RANGE_8_G:
            std::cout << "+-8G" << std::endl;
            break;
        case ICM20948::ACCEL_RANGE_16_G:
            std::cout << "+-16G" << std::endl;
            break;
    }
    // icm->setGyroRange(GYRO_RANGE_2000_DPS);
    std::cout << "Gyro range set to: ";
    switch (icm->getGyroRange()) {
        case ICM20948::GYRO_RANGE_250_DPS:
            std::cout << "250 degrees/s" << std::endl;
            break;
        case ICM20948::GYRO_RANGE_500_DPS:
            std::cout << "500 degrees/s" << std::endl;
            break;
        case ICM20948::GYRO_RANGE_1000_DPS:
            std::cout << "1000 degrees/s" << std::endl;
            break;
        case ICM20948::GYRO_RANGE_2000_DPS:
            std::cout << "2000 degrees/s" << std::endl;
            break;
    }

    //  icm->setAccelRateDivisor(4095);
    unsigned int accel_divisor = icm->getAccelRateDivisor();
    float accel_rate = 1125 / (1.0 + accel_divisor);

    std::cout << "Accelerometer data rate divisor set to: " << accel_divisor << std::endl;
    std::cout << "Accelerometer data rate is approximately: " << accel_rate << " Hz" << std::endl;

    //  icm->setGyroRateDivisor(255);
    unsigned int gyro_divisor = icm->getGyroRateDivisor();
    float gyro_rate = 1100 / (1.0 + gyro_divisor);

    std::cout << "Gyro data rate divisor set to: " << gyro_divisor << std::endl;
    std::cout << "Gyro data rate is approximately: " << gyro_rate << " Hz" << std::endl;

    // icm->setMagDataRate(AK09916_MAG_DATARATE_100_HZ);
    std::cout << "Magnetometer data rate set to: ";
    switch (icm->getMagDataRate()) {
        case ICM20948::AK09916_MAG_DATARATE_SHUTDOWN:
            std::cout << "Shutdown" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_SINGLE:
            std::cout << "Single/One shot" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_10_HZ:
            std::cout << "10 Hz" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_20_HZ:
            std::cout << "20 Hz" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_50_HZ:
            std::cout << "50 Hz" << std::endl;
            break;
        case ICM20948::AK09916_MAG_DATARATE_100_HZ:
            std::cout << "100 Hz" << std::endl;
            break;
    }
}

void Controller::initBuffer(ICM20948::ICM20948* icm) {

    for(u_char i = 0; i < buffersize; ++i) {
        getAlignment(icm);
        Util::sleep(10);
    }
}

void Controller::calcDerivation(ICM20948::ICM20948* icm) {

    float acc_derivations_list[testsize][3],
    //      mag_derivations_list[testsize][3],
          gyro_derivations_list[testsize][3];

    for (int i = 0; i < testsize; ++i) {
        sensors_event_t accel;
        sensors_event_t gyro;
        sensors_event_t mag;
        icm->getEvent(0, &accel, &gyro, &mag);
        acc_derivations_list[i][0] = accel.acceleration.x;
        acc_derivations_list[i][1] = accel.acceleration.y;
        acc_derivations_list[i][2] = accel.acceleration.z;
        gyro_derivations_list[i][0] = gyro.gyro.x;
        gyro_derivations_list[i][1] = gyro.gyro.y;
        gyro_derivations_list[i][2] = gyro.gyro.z;
        /*mag_derivations_list[i][0] = mag.magnetic.x;
        mag_derivations_list[i][1] = mag.magnetic.y;
        mag_derivations_list[i][2] = mag.magnetic.z;*/
    }
    float sumx = .0f, sumy = .0f, sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += acc_derivations_list[i][0];
        sumy += acc_derivations_list[i][1];
        sumz += acc_derivations_list[i][2];
    }
    acc_derivation[0] = sumx / testsize;
    acc_derivation[1] = sumy / testsize;
    acc_derivation[2] = sumz / testsize - gravity;

    sumx = sumy = sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += gyro_derivations_list[i][0];
        sumy += gyro_derivations_list[i][1];
        sumz += gyro_derivations_list[i][2];
    }
    gyro_derivation[0] = sumx / testsize;
    gyro_derivation[1] = sumy / testsize;
    gyro_derivation[2] = sumz / testsize;
    /*
    sumx = sumy = sumz = .0f;
    for (int i = 0; i < testsize; ++i) {
        sumx += mag_derivations_list[i][0];
        sumy += mag_derivations_list[i][1];
        sumz += mag_derivations_list[i][2];
    }
    mag_derivation[0] = sumx / testsize;
    mag_derivation[1] = sumy / testsize;
    mag_derivation[2] = sumz / testsize;
    */

    std::cout << "The acceleration derivation is: (" << acc_derivation[0] << ", " << acc_derivation[1] << ", " << acc_derivation[2] << ")" << std::endl;
    std::cout << "The gyro derivation is: (" << gyro_derivation[0] << ", " << gyro_derivation[1] << ", " << gyro_derivation[2] << ")" << std::endl;
    //std::cout << "The magnetometer derivation is: (" << mag_derivation[0] << ", " << mag_derivation[1] << ", " << mag_derivation[2] << ")" << std::endl;
}

void Controller::getAlignment(ICM20948::ICM20948* icm) {
    sensors_event_t accel;
    sensors_event_t gyro;
    sensors_event_t mag;
    sensors_event_t temp;
    icm->getEvent(0, &accel, &gyro, &mag);

    lastAcc[bufferpos][0] = accel.acceleration.x - acc_derivation[0];
    lastAcc[bufferpos][1] = accel.acceleration.y - acc_derivation[1];
    lastAcc[bufferpos][2] = accel.acceleration.z - acc_derivation[2];
    lastGyro[bufferpos][0] = gyro.gyro.x - gyro_derivation[0];
    lastGyro[bufferpos][1] = gyro.gyro.y - gyro_derivation[1];
    lastGyro[bufferpos][2] = gyro.gyro.z - gyro_derivation[2];
    lastMag[bufferpos][0] = (mag.magnetic.x+((106.8/2)-30.15))*106.8/98;
    lastMag[bufferpos][1] = (mag.magnetic.y+((106.5/2)-37.65))*106.5/98;
    lastMag[bufferpos][2] = (mag.magnetic.z+((103.2/2)-50.85))*103.2/98;

    ++bufferpos %= buffersize;
}

void Controller::calcAlignment(void) {

    float acc[3]={.0f,.0f,.0f}, mag[3]={.0f,.0f,.0f};
    for (char i = 0; i < buffersize; ++i) {
        acc[0] += (i+1) * lastAcc[((bufferpos+i)%buffersize)][0];
        acc[1] += (i+1) * lastAcc[((bufferpos+i)%buffersize)][1];
        acc[2] += (i+1) * lastAcc[((bufferpos+i)%buffersize)][2];
        mag[0] += (i+1) * lastMag[((bufferpos+i)%buffersize)][0];
        mag[1] += (i+1) * lastMag[((bufferpos+i)%buffersize)][1];
        mag[2] += (i+1) * lastMag[((bufferpos+i)%buffersize)][2];
    }

    pitch_rad = -atan2(lastAcc[bufferpos][0], sqrt(pow(lastAcc[bufferpos][1], 2) + pow(lastAcc[bufferpos][2], 2)));
    roll_rad = atan2(lastAcc[bufferpos][1], lastAcc[bufferpos][2]);
    float xh = lastMag[bufferpos][0]*cos(pitch_rad)+lastMag[bufferpos][1]*sin(roll_rad)*sin(pitch_rad)-lastMag[bufferpos][2]*cos(roll_rad)*sin(pitch_rad);
    float yh = lastMag[bufferpos][1]*cos(roll_rad)+lastMag[bufferpos][2]*sin(roll_rad);
    heading_rad = atan2(yh, xh);
    float pitch = pitch_rad * (180.0 / PI);
    float roll = roll_rad * (180.0 / PI);
    float heading = heading_rad * (180.0 / PI);

    /*
    std::cout << "*******************" << std::endl;
    // std::cout << "\t\tTemperature: " << temp.temperature << " deg C" << std::endl;
    std::cout << "Accel X: " << corrected_acc[0] << " \tY: " << corrected_acc[1] << " \tZ: " << corrected_acc[2] << " m/s^2 " << std::endl;
    std::cout << "Gyro X: " << corrected_gyro[0] << " \tY: " << corrected_gyro[1] << " \tZ: " << corrected_gyro[2] << " radians/s " << std::endl;
    std::cout << "Mag X: " << mag.magnetic.x << " \tY: " << mag.magnetic.y << " \tZ: " << mag.magnetic.z << " uT" << std::endl;
    std::cout << "Pitch: " << pitch_rad << "rad" << std::endl;
    std::cout << "Roll: " << roll_rad << "rad" << std::endl;
    std::cout << "yh: " << yh << ", xh: " << xh << std::endl;
    std::cout << "Heading rad: " << heading_rad << std::endl;*/
    std::cout << "Pitch: " << pitch << "°" << std::endl;
    std::cout << "Roll: " << roll << "°" << std::endl;
    std::cout << "Heading: " << heading << "°" << std::endl;
    
}


int main(void) {
    // Set decimal precision of cout
    std::cout.precision(4);

    Controller esc;
    // Init devices
    PCA9685::PCA9685 pwm = PCA9685::PCA9685(PCA9685::I2C_ADDRESS);
    ICM20948::ICM20948 icm = ICM20948::ICM20948(ICM20948::I2C_ADDRESS);
    
    // Setup
    int result = pwm.begin();
    std::cout << "Begin PWM: " << result << std::endl;
    if (result < 0) { return -1; }
    result = icm.begin();
    std::cout << "Begin ICM: " << result << std::endl;
    if (result < 1) { return result; }

    // Set PWM Freq to 50Hz
    pwm.setPWMFreq(50);

    esc.initPPM(&pwm);
    //esc.testPPM(&pwm);

    esc.calcDerivation(&icm);

    esc.initBuffer(&icm);

    esc.initDuty(&pwm);

    bool b = false;
    for (u_short i = 0x02FF; i; --i) {

        esc.getAlignment(&icm);

        if (b) {
            esc.calcAlignment();
            esc.setDuty(&pwm);
        }
        b = !b;

        Util::sleep(10);
    }

    esc.stopDuty(&pwm);

    esc.stopPPM(&pwm);

    return 0;
}
