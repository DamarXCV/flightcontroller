#include <pigpio.h>
#include <chrono>
#include <thread>
#include <iostream>

#include "MPL3115A2.h"
#include "../Util/Helper.h"

namespace MPL3115A2 {
/*!
 *  @brief  Instantiates a new MPL3115A2 class
 *  @param  addr
 *          The 7-bit I2C address to locate this chip, default is 0x60
*/
MPL3115A2::MPL3115A2(unsigned int addr) : I2CDevice(addr) {}

/*!
 *  @brief  Setup the HW (reads coefficients values, etc.)
 *  @return False on successful setup, error code otherwise
 */
int MPL3115A2::begin(bool initGpio) {
    // init the i2c device
    int begin_response = I2CDevice::begin(initGpio);
    // Check if an error occurred aka response < 0
    if (begin_response < 0) { return begin_response; }
    
    // Check if the device is working
    int who_am_i_response = read(REG_WHO_AM_I);
    // check if response is correct 
    if (who_am_i_response != WHO_AM_I_RESPONSE) { return who_am_i_response; }

    // Software reset the device
    write(REG_CTRL_REG1, REG_CTRL_REG1_BIT_RST);
    Util::sleep(10);

    // Wait till software reset is done aka RST is reset
    while (read(REG_CTRL_REG1) & REG_CTRL_REG1_BIT_RST)
        Util::sleep(10);

    // Update control register 1 with oversample ratio and altimeter/barometer mode
    write(REG_CTRL_REG1, (REG_CTRL_REG1_BIT_OS_VAL_128 | REG_CTRL_REG1_BIT_ALT));

    // Enable raise event flag on new on new pressure/altitude and/or temperature data
    write(REG_PT_DATA_CFG,
        REG_PT_DATA_CFG_BIT_TDEFE | REG_PT_DATA_CFG_BIT_PDEFE | REG_PT_DATA_CFG_BIT_DREM);

    return 0;
}

/*!
 *  @brief  Close all connections
 *  @param  terminateGpio
 *          True if gpioTerminate should be called
 *  @return False on successful setup, error code otherwise
 */
int MPL3115A2::end(bool terminateGpio) {
    // End the i2c device
    return I2CDevice::end(terminateGpio);
}

/*!
 *  @brief  Gets the floating-point pressure level in kPa
 *  @param  value
 *          Pointer to the variable, in which the result gets stored
 *  @return 0 if success else error code
 */
int MPL3115A2::getPressure(float* value) {
    // Wait if the device is currently measuring
    while (read(REG_CTRL_REG1) & REG_CTRL_REG1_BIT_OST)
        Util::sleep(10);


    // Set mode to barometer and initiate a new immediat measurement
    unsigned int mode = read(REG_CTRL_REG1);
    mode &= ~REG_CTRL_REG1_BIT_ALT;
    mode |= REG_CTRL_REG1_BIT_OST;
    write(REG_CTRL_REG1, mode);

    // Wait till new pressure/altitude data is available
    unsigned int sta = 0;
    while (!(sta & REG_STATUS_BIT_PDR)) {
        sta = read(REG_STATUS);
        Util::sleep(10);
    }

    // Get the new data
    char* buffer = new char[3];
    int response = readBlock(REG_OUT_P_MSB, buffer, 3);
    if (response < 0) { return response; }

    // Extract value from buffer
    unsigned int pressure = buffer[0];
    pressure <<= 8;
    pressure |= buffer[1];
    pressure <<= 8;
    pressure |= buffer[2];
    pressure >>= 4;

    // Store the result
    *value = (float(pressure) / 4.0f);

    return 0;
}

/*!
 *  @brief  Gets the floating-point altitude value
 *  @param  value
 *          Pointer to the variable, in which the result gets stored
 *  @return 0 if success else error code
 */
int MPL3115A2::getAltitude(float* value) {
    // Wait if the device is currently measuring
    while (read(REG_CTRL_REG1) & REG_CTRL_REG1_BIT_OST)
        Util::sleep(10);

    unsigned int mode = read(REG_CTRL_REG1);
    mode |= REG_CTRL_REG1_BIT_ALT;
    mode |= REG_CTRL_REG1_BIT_OST;
    // Set mode to altimeter and get a new immediat measurement
    write(REG_CTRL_REG1, mode);

    // Wait till new pressure/altitude data is available
    unsigned int sta = 0;
    while (!(sta & REG_STATUS_BIT_PDR)) {
        sta = read(REG_STATUS);
        Util::sleep(10);
    }

    // Get the new data
    char* buffer = new char[3];
    int response = readBlock(REG_OUT_P_MSB, buffer, 3);
    if (response < 0) { return response; }

    // Extract value from buffer
    unsigned int shifter = 0;
    shifter |= buffer[0]; 
    shifter <<= 8;
    shifter |= buffer[1];
    shifter <<= 8;
    shifter |= buffer[2];
    shifter >>= 4;
    float altitude = ((float)shifter / 16.0f);

    // Handle negative values
    if (altitude >= 32768.0) { altitude -= 65536.0; }

    // Store the result
    *value = altitude;
    return 0;
}

/*!
 *  @brief  Gets the floating-point temperature in Centigrade
 *  @param  value
 *          Pointer to the variable, in which the result gets stored
 *  @return 0 if success else error code
 */
int MPL3115A2::getTemperature(float* value) {
    // Wait if the device is currently measuring
    while (read(REG_CTRL_REG1) & REG_CTRL_REG1_BIT_OST)
        Util::sleep(10);

    // Check if new temperature data is available
    if (!(read(REG_STATUS) & REG_STATUS_BIT_TDR))
    {
        unsigned int mode = read(REG_CTRL_REG1);
        mode |= REG_CTRL_REG1_BIT_OST;
        // Get a new immediat measurement
        write(REG_CTRL_REG1, mode);

        // Wait till new pressure/altitude data is available
        unsigned int sta = 0;
        while (!(sta & REG_STATUS_BIT_PDR)) {
            sta = read(REG_STATUS);
            Util::sleep(10);
        }
        // Clear the pressure/altitude storage
        readBlock(REG_OUT_P_MSB, new char[3], 3);
    }

    // Get the new data
    char* buffer = new char[2];
    int response = readBlock(REG_OUT_T_MSB, buffer, 2);
    if (response < 0) { return response; }

    unsigned int temp = buffer[0];
    temp <<= 8;
    temp |= buffer[1];
    temp |= buffer[2];
    temp >>= 4;

    if (temp & 0x800) { temp |= 0xF000; }
    *value = (temp / 16.0);

    return 0;
}

/*!
 *  @brief  Set the local sea level barometric pressure
 *  @param  pascal 
 *          Values between 0Pa and 131.068Pa are possible. Values are saved in 2Pa/Unit steps
 *  @return 0 if success else error code
 */
int MPL3115A2::setSeaPressure(unsigned int pascal) {
    unsigned int pascal_fraction = pascal / 2;

    // Set the first 8Bits
    int response = write(REG_BAR_IN_MSB, (pascal_fraction >> 8));
    if (response < 0) { return response; }

    // Set the last 8Bits
    return write(REG_BAR_IN_LSB, (pascal_fraction & 255));
}

/*!
 *  @brief  Set the Offset for the pressure output
 *  @param  pascal
 *          Values between −512Pa and +508Pa are possible. Values are saved in 4Pa/Unit steps
 *  @return 0 if success else error code
 */
int MPL3115A2::setPressureOffset(int pascal) {
    return write(REG_OFF_P, ((char) (pascal / 4)));
}

/*!
 *  @brief  Set the Offset for the altitude output 
 *  @param  meters
 *          Values between −128m and +127m are possible
 *  @return 0 if success else error code
 */
int MPL3115A2::setAltitudeOffset(char meters) {
    return write(REG_OFF_H, meters);
}

/*!
 *  @brief  Set the Offset for the temperature output
 *  @param  celcius
 *          Values between −8°C and +7.9375°C are possible. Values are saved in 0.0625°C/Unit steps
 *  @return 0 if success else error code
 */
int MPL3115A2::setTemperatureOffset(float celcius) {
    return write(REG_OFF_T, ((char) (celcius / 0.0625f)));
}
} // namespace MPL3115A2
