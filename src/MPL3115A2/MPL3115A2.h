
#ifndef __MPL3115A2__
#define __MPL3115A2__

#include "../Util/I2CDevice.h"

typedef const unsigned int c_uint;

namespace MPL3115A2 { 
static c_uint I2C_ADDRESS = 0x60;
static c_uint WHO_AM_I_RESPONSE = 0xC4;

#pragma region /** MPL3115A2 registers **/
    static c_uint REG_STATUS = 0x00; // Sensor status register

    static c_uint REG_OUT_P_MSB = 0x01; // Pressure data out MSB
    static c_uint REG_OUT_P_CSB = 0x02; // Pressure data out CSB
    static c_uint REG_OUT_P_LSB = 0x03; // Pressure data out LSB

    static c_uint REG_OUT_T_MSB = 0x04; // Temperature data out MSB
    static c_uint REG_OUT_T_LSB = 0x05; // Temperature data out LSB

    static c_uint REG_DR_STATUS = 0x06; // Sensor status register

    static c_uint REG_OUT_P_DELTA_MSB = 0x07; // Pressure data out delta MSB
    static c_uint REG_OUT_P_DELTA_CSB = 0x08; // Pressure data out delta CSB
    static c_uint REG_OUT_P_DELTA_LSB = 0x09; // Pressure data out delta LSB
    static c_uint REG_OUT_T_DELTA_MSB = 0x0A; // Temperature data out delta MSB
    static c_uint REG_OUT_T_DELTA_LSB = 0x0B; // Temperature data out delta LSB

    static c_uint REG_WHO_AM_I = 0x0C; // Device identification register

    static c_uint REG_F_STATUS   = 0x0D; // FIFO status register
    static c_uint REG_F_DATA     = 0x0E; // FIFO 8-bit data access
    static c_uint REG_F_SETUP    = 0x0F; // FIFO setup register

    static c_uint REG_TIME_DLY   = 0x10; // Time delay register
    static c_uint REG_SYSMOD     = 0x11; // System mode register
    static c_uint REG_NT_SOURCE  = 0x12; // Interrupt source register
    static c_uint REG_PT_DATA_CFG= 0x13; // PT data configuration register

    static c_uint REG_BAR_IN_MSB = 0x14; // BAR input in MSB
    static c_uint REG_BAR_IN_LSB = 0x15; // BAR input in LSB

    static c_uint REG_P_TGT_MSB  = 0x16; // Pressure target MSB
    static c_uint REG_P_TGT_LSB  = 0x17; // Pressure target LSB
    static c_uint REG_T_TGT      = 0x18; // Temperature target register
    static c_uint REG_P_WND_MSB  = 0x19; // Pressure/altitude window MSB
    static c_uint REG_P_WND_LSB  = 0x1A; // Pressure/altitude window LSB
    static c_uint REG_T_WND      = 0x1B; // Temperature window register
    static c_uint REG_P_MIN_MSB  = 0x1C; // Minimum pressure data out MSB
    static c_uint REG_P_MIN_CSB  = 0x1D; // Minimum pressure data out CSB
    static c_uint REG_P_MIN_LSB  = 0x1E; // Minimum pressure data out LSB
    static c_uint REG_T_MIN_MSB  = 0x1F; // Minimum temperature data out MSB
    static c_uint REG_T_MIN_LSB  = 0x20; // Minimum temperature data out LSB
    static c_uint REG_P_MAX_MSB  = 0x21; // Maximum pressure data out MSB
    static c_uint REG_P_MAX_CSB  = 0x22; // Maximum pressure data out CSB
    static c_uint REG_P_MAX_LSB  = 0x23; // Maximum pressure data out LSB
    static c_uint REG_T_MAX_MSB  = 0x24; // Maximum temperature data out MSB
    static c_uint REG_T_MAX_LSB  = 0x25; // Maximum temperature data out LSB

    static c_uint REG_CTRL_REG1  = 0x26; // Control register 1
    static c_uint REG_CTRL_REG2  = 0x27; // Control register 2
    static c_uint REG_CTRL_REG3  = 0x28; // Control register 3
    static c_uint REG_CTRL_REG4  = 0x29; // Control register 4
    static c_uint REG_CTRL_REG5  = 0x2A; // Control register 5

    static c_uint REG_OFF_P = 0x2B; // Pressure data user offset register
    static c_uint REG_OFF_T = 0x2C; // Temperature data user offset register
    static c_uint REG_OFF_H = 0x2D; // Altitude data user offset register
#pragma endregion

#pragma region /** MPL3115A2 STATUS (DR_STATUS/F_STATUS) register bits **/
    // Reserved - 0b0000'0001 - 0x01
    static c_uint REG_STATUS_BIT_TDR = 0x02; // 0b0000'0010
    static c_uint REG_STATUS_BIT_PDR = 0x04; // 0b0000'0100
    static c_uint REG_STATUS_BIT_PTDR= 0x08; // 0b0000'1000
    // Reserved - 0b0001'0000 - 0x10
    static c_uint REG_STATUS_BIT_TOW = 0x20; // 0b0010'0000
    static c_uint REG_STATUS_BIT_POW = 0x40; // 0b0100'0000
    static c_uint REG_STATUS_BIT_PTOW= 0x80; // 0b1000'0000
#pragma endregion

#pragma region /** MPL3115A2 F_STATUS register bits **/
    // F_CNT - 0b0000'0001 - 0x01 -> 0b0010'0000 - 0x20
    static c_uint REG_F_STATUS_BIT_F_OVF         = 0x80; // 0b1000'0000
    static c_uint REG_F_STATUS_BIT_F_WMRK_FLAG   = 0x40; // 0b0100'0000
#pragma endregion

#pragma region /** MPL3115A2 SYSMOD register bits **/
    static c_uint REG_SYSMOD_BIT_SYSMOD   = 0x01; // 0b0000'0001
    // Reserved - 0b0000'0010 - 0x02
    // Reserved - 0b0000'0100 - 0x04
    // Reserved - 0b0000'1000 - 0x08
    // Reserved - 0b0001'0000 - 0x10
    // Reserved - 0b0010'0000 - 0x20
    // Reserved - 0b0100'0000 - 0x40
    // Reserved - 0b1000'0000 - 0x80
#pragma endregion

#pragma region /** MPL3115A2 INT_SOURCE register bits **/
    static c_uint REG_INT_SOURCE_BIT_SRC_TCHG = 0x01; // 0b0000'0001
    static c_uint REG_INT_SOURCE_BIT_SRC_PCHG = 0x02; // 0b0000'0010
    static c_uint REG_INT_SOURCE_BIT_SRC_TTH  = 0x04; // 0b0000'0100
    static c_uint REG_INT_SOURCE_BIT_SRC_PTH  = 0x08; // 0b0000'1000
    static c_uint REG_INT_SOURCE_BIT_SRC_TW   = 0x10; // 0b0001'0000
    static c_uint REG_INT_SOURCE_BIT_SRC_PW   = 0x20; // 0b0010'0000
    static c_uint REG_INT_SOURCE_BIT_SRC_FIFO = 0x40; // 0b0100'0000
    static c_uint REG_INT_SOURCE_BIT_SRC_DRDY = 0x80; // 0b1000'0000
#pragma endregion

#pragma region /** MPL3115A2 PT_DATA_CFG register bits **/
    static c_uint REG_PT_DATA_CFG_BIT_TDEFE   = 0x01; // 0b0000'0001
    static c_uint REG_PT_DATA_CFG_BIT_PDEFE   = 0x02; // 0b0000'0010
    static c_uint REG_PT_DATA_CFG_BIT_DREM    = 0x04; // 0b0000'0100
    // Reserved - 0b0000'1000 - 0x08
    // Reserved - 0b0001'0000 - 0x10
    // Reserved - 0b0010'0000 - 0x20
    // Reserved - 0b0100'0000 - 0x40
    // Reserved - 0b1000'0000 - 0x80
#pragma endregion

#pragma region /** MPL3115A2 CTRL_REG1 register bits **/
    static c_uint REG_CTRL_REG1_BIT_SBYB  = 0x01; // 0b0000'0001
    static c_uint REG_CTRL_REG1_BIT_OST   = 0x02; // 0b0000'0010
    static c_uint REG_CTRL_REG1_BIT_RST   = 0x04; // 0b0000'0100
    // OS - 0b0000'1000 - 0x08 -> 0b0010'0000 - 0x20
    // Reserved - 0b0100'0000 - 0x40
    static c_uint REG_CTRL_REG1_BIT_ALT   = 0x80; // 0b1000'0000
#pragma endregion

#pragma region /** MPL3115A2 oversample values with shift **/
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_1     = 0x00; // 0b0000'0000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_2     = 0x08; // 0b0000'1000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_4     = 0x10; // 0b0001'0000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_8     = 0x18; // 0b0001'1000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_16    = 0x20; // 0b0010'0000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_32    = 0x28; // 0b0010'1000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_64    = 0x30; // 0b0011'0000
    static c_uint REG_CTRL_REG1_BIT_OS_VAL_128   = 0x38; // 0b0011'1000
#pragma endregion

#pragma region /** MPL3115A2 CTRL_REG2 register bits **/
    // ST - 0b0000'0001 - 0x01 -> 0b0000'1000 - 0x80
    static c_uint REG_CTRL_REG2_BIT_ALARM_SEL     = 0x10; // 0b0001'0000
    static c_uint REG_CTRL_REG2_BIT_LOAD_OUTPUT   = 0x20; // 0b0010'0000
    // Reserved - 0b0100'0000 - 0x40
    // Reserved - 0b1000'0000 - 0x80
#pragma endregion

#pragma region /** MPL3115A2 CTRL_REG3 register bits **/
    static c_uint REG_CTRL_REG3_BIT_PP_OD2= 0x01; // 0b0000'0001
    static c_uint REG_CTRL_REG3_BIT_IPOL2 = 0x02; // 0b0000'0010
    // Reserved - 0b0000'0400 - 0x04
    // Reserved - 0b0000'1000 - 0x08
    static c_uint REG_CTRL_REG3_BIT_PP_OD1= 0x10; // 0b0001'0000
    static c_uint REG_CTRL_REG3_BIT_IPOL1 = 0x20; // 0b0010'0000
    // Reserved - 0b0100'0000 - 0x40
    // Reserved - 0b1000'0000 - 0x80
#pragma endregion

#pragma region /** MPL3115A2 CTRL_REG4 register bits **/
    static c_uint REG_CTRL_REG4_BIT_INT_EN_TCHG   = 0x01; // 0b0000'0001
    static c_uint REG_CTRL_REG4_BIT_INT_EN_PCHG   = 0x02; // 0b0000'0010
    static c_uint REG_CTRL_REG4_BIT_INT_EN_TTH    = 0x04; // 0b0000'0100
    static c_uint REG_CTRL_REG4_BIT_INT_EN_PTH    = 0x08; // 0b0000'1000
    static c_uint REG_CTRL_REG4_BIT_INT_EN_TW     = 0x10; // 0b0001'0000
    static c_uint REG_CTRL_REG4_BIT_INT_EN_PW     = 0x20; // 0b0010'0000
    static c_uint REG_CTRL_REG4_BIT_INT_EN_FIFO   = 0x40; // 0b0100'0000
    static c_uint REG_CTRL_REG4_BIT_INT_EN_DRDY   = 0x80; // 0b1000'0000
#pragma endregion

#pragma region /** MPL3115A2 CTRL_REG5 register bits **/
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_TCHG   = 0x01; // 0b0000'0001
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_PCHG   = 0x02; // 0b0000'0010
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_TTH    = 0x04; // 0b0000'0100
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_PTH    = 0x08; // 0b0000'1000
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_TW     = 0x10; // 0b0001'0000
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_PW     = 0x20; // 0b0010'0000
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_FIFO   = 0x40; // 0b0100'0000
    static c_uint REG_CTRL_REG5_BIT_INT_CFG_DRDY   = 0x80; // 0b1000'0000
#pragma endregion

/*!
 *  @brief  Class that stores state and functions for interacting with MPL3115A2
 * altimeter
 */
class MPL3115A2 : I2CDevice {
public:
    MPL3115A2(unsigned int addr);
    int begin(bool initGpio = true);
    int end(bool terminateGpio = true);

    int getPressure(float* value);
    int getAltitude(float* value);
    int getTemperature(float* value);

    int setSeaPressure(unsigned int pascal);
    int setPressureOffset(int pascal);
    int setAltitudeOffset(char meters);
    int setTemperatureOffset(float celcius);
};

} // namespace MPL3115A2

#endif
