#ifndef TELEMETRY_H
#define TELEMETRY_H

#include <pigpio.h>

namespace Telemetry {

/*!
 *  @brief  
 */
class Telemetry {
public:
    Telemetry();
    int begin();
private:
    int _handler;
};

} // namespace Telemetry

#endif // TELEMETRY_H
