#include <algorithm>
#include <iostream>

#include "Telemetry.h"
#include "./../Util/Helper.h"

namespace Telemetry {
/*!
 *  @brief  
 */
Telemetry::Telemetry() {
    begin();
}

int Telemetry::begin(void) {
    // Initialise pigpio, returns the pigpio version number if OK, otherwise PI_INIT_FAILED(-1)
    if (gpioInitialise() < 0) { return PI_INIT_FAILED; }

    // gpioSetMode(23, PI_)

    char ser_name[] = "/dev/serial0"; 

    int response = serOpen(ser_name, 115200, 0);
    
    std::cout << "response serOpen: " << response << std::endl;

    // Check if the handle is a error code aka < 0, if so return the error code
    if (response < 0) { return response; }

    // Set the handler for the instance
    _handler = response;

    for (size_t i = 0; i < 200; i++)
    {
        
        response = serDataAvailable(_handler);
        // response = serRead(_handler, buffer, 10);
        std::cout << "response serRead: " << response << std::endl;
        // std::cout << "buffer serRead: " << buffer << std::endl;
        Util::sleep(100);
    }

    return 0;
}

} // namespace PCA9685
