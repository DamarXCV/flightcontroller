#include <algorithm>
#include <iostream>

#include "PCA9685.h"
#include "./../Util/Helper.h"

namespace PCA9685 {
/*!
 *  @brief  Instantiates a new PCA9685 PWM driver chip with the I2C address
 *  @param  addr
 *          The 7-bit I2C address to locate this chip, default is 0x40
 */
PCA9685::PCA9685(unsigned int addr) : I2CDevice(addr) {}

/*!
 *  @brief  Setups the I2C interface and hardware
 *  @param  initGpio
 *          True if gpioInitialise should be called
 *  @return False on successful setup, error code otherwise
 */
int PCA9685::begin(bool initGpio) {
    int begin_res = I2CDevice::begin(initGpio);
    if (begin_res < 0) { return begin_res; }
    
    reset();
    
    return 0;
}

/*!
 *  @brief  Close all connections
 *  @param  terminateGpio
 *          True if gpioTerminate should be called
 *  @return False on successful setup, error code otherwise
 */
int PCA9685::end(bool terminateGpio) {
    // End the i2c device
    return I2CDevice::end(terminateGpio);
}

/*!
 *  @brief  Sends a reset command to the PCA9685 chip over I2C
 */
void PCA9685::reset() {
    write(REG_MODE1, REG_MODE1_BIT_RESTART);
    Util::sleep(10);
}

/*!
 *  @brief  Puts board into sleep mode
 */
void PCA9685::sleep() {
    unsigned int mode = read(REG_MODE1);
    mode |= REG_MODE1_BIT_SLEEP;
    write(REG_MODE1, mode);
    Util::sleep(5);
}

/*!
 *  @brief  Wakes board from sleep
 */
void PCA9685::wakeup() {
    unsigned int mode = read(REG_MODE1);
    mode &= ~REG_MODE1_BIT_SLEEP;
    write(REG_MODE1, mode);
}

/*!
 *  @brief  Sets EXTCLK pin to use the external clock
 *  @param  prescale
 *          Configures the prescale value to be used by the external clock
 */
void PCA9685::setExtClk(unsigned int prescale) {
    unsigned int mode = read(REG_MODE1);
    
    // go to sleep, turn off internal oscillator
    mode |= REG_MODE1_BIT_SLEEP;
    write(REG_MODE1, mode); 

    // This sets both the SLEEP and EXTCLK bits of the MODE1 register to switch to
    // use the external clock.
    mode |= REG_MODE1_BIT_EXTCLK;
    write(REG_MODE1, mode);

    // set the prescaler
    write(REG_PRESCALE, prescale);

    Util::sleep(5);
    // clear the SLEEP bit
    mode &= ~REG_MODE1_BIT_SLEEP;
    write(REG_MODE1, mode);
}

/*!
 *  @brief  Sets the PWM frequency for the entire chip, up to ~1.6 KHz
 *  @param  freq
 *          Floating point frequency that we will attempt to match
 */
void PCA9685::setPWMFreq(float freq) {
    // Datasheet limit, 7.3.5
    if (freq < 24) { freq = 24; }
    if (freq > 1526) { freq = 1526; }

    // Calculate prescalse value
    float prescale_val = ((FREQUENCY_OSCILLATOR / (4096.0 * freq))) - 1;
    if (prescale_val < PRESCALE_MIN) { prescale_val = PRESCALE_MIN; }
    if (prescale_val > PRESCALE_MAX) { prescale_val = PRESCALE_MAX; }
    unsigned int prescale = (unsigned int)prescale_val;

    // Set prescalse value
    unsigned int oldmode = read(REG_MODE1);
    unsigned int newmode = oldmode | REG_MODE1_BIT_SLEEP;
    write(REG_MODE1, newmode);
    write(REG_PRESCALE, prescale);
    write(REG_MODE1, oldmode);
    Util::sleep(5);
}

/*!
 *  @brief  Reads set Prescale from PCA9685
 *  @return prescale value
 */
unsigned int PCA9685::readPrescale(void) {
    return read(REG_PRESCALE);
}

/*!
 *  @brief  Gets the PWM output of one of the PCA9685 pins
 *  @param  pin
 *          One of the PWM output pins, from 0 to 15
 *  @return requested PWM output value
 */
unsigned int PCA9685::getPWM(unsigned int pin) {
    char* buffer = new char[4];
    readBlock(REG_LED0_ON_L + 4 * pin, buffer, 4);

    unsigned int result = (unsigned int) buffer;
    return result;
}

/*!
 *  @brief  Sets the PWM output of one of the PCA9685 pins
 *  @param  pin
 *          One of the PWM output pins, from 0 to 15
 *  @param  on
 *          At what point in the 4096-part cycle to turn the PWM output ON
 *  @param  off
 *          At what point in the 4096-part cycle to turn the PWM output OFF
 */
void PCA9685::setPWM(unsigned int pin, unsigned int on, unsigned int off) {
    write(REG_LED0_ON_L + 4 * pin, on & 0xFF);
    write(REG_LED0_ON_H + 4 * pin, on >> 8);

    write(REG_LED0_OFF_L + 4 * pin, off & 0xFF);
    write(REG_LED0_OFF_H + 4 * pin, off >> 8);
}

/*!
 *  @brief  Sets PPM output of one of the PCA9685 pins
 *  @param  pin
 *          One of the PWM output pins, from 0 to 15
 *  @param  duty
 *          The percentage of interval activation  
 */
void PCA9685::setPPM(unsigned int pin, float duty) {
    PCA9685::setPWM(pin, 0, (unsigned int) (4096 * (duty / 100.0f)));
}

/*!
 *  @brief  Sets Servo PPM output of one of the PCA9685 pins Servos usaly work
 *          with a 50 Hz freq and use signals with 1ms to 2ms length
 *  @param  pin
 *          One of the PWM output pins, from 0 to 15
 *  @param  duty
 *          The percentage of interval activation  
 */
void PCA9685::setServoPPM(unsigned int pin, float duty) {
    // Steps from 5% aka 1ms to 10% aka 2ms
    PCA9685::setPPM(pin, 5.0f + (duty * 0.05f));
}
} // namespace PCA9685
