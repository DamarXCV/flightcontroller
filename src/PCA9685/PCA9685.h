#ifndef _PCA9685_H
#define _PCA9685_H

#include "../Util/I2CDevice.h"

typedef const unsigned int c_uint;

namespace PCA9685 {
static c_uint I2C_ADDRESS = 0x40;
static c_uint FREQUENCY_OSCILLATOR = 25'000'000;

static c_uint PRESCALE_MIN = 3;   // minimum prescale value
static c_uint PRESCALE_MAX = 255; // maximum prescale value

#pragma region /** PCA9685 registers **/
    static c_uint REG_MODE1 = 0x00;      // Mode Register 1
    static c_uint REG_MODE2 = 0x01;      // Mode Register 2
    
    static c_uint REG_SUBADR1 = 0x02;    // I2C-bus subaddress 1
    static c_uint REG_SUBADR2 = 0x03;    // I2C-bus subaddress 2
    static c_uint REG_SUBADR3 = 0x04;    // I2C-bus subaddress 3
    
    static c_uint REG_ALLCALLADR = 0x05; // LED All Call I2C-bus address

    static c_uint REG_LED0_ON_L = 0x06;  // LED0 on tick, low byte
    static c_uint REG_LED0_ON_H = 0x07;  // LED0 on tick, high byte
    static c_uint REG_LED0_OFF_L = 0x08; // LED0 off tick, low byte
    static c_uint REG_LED0_OFF_H = 0x09; // LED0 off tick, high byte

    static c_uint REG_LED1_ON_L = 0x0A;  // LED1 on tick, low byte
    static c_uint REG_LED1_ON_H = 0x0B;  // LED1 on tick, high byte
    static c_uint REG_LED1_OFF_L = 0x0C; // LED1 off tick, low byte
    static c_uint REG_LED1_OFF_H = 0x0D; // LED1 off tick, high byte

    static c_uint REG_LED2_ON_L = 0x0E;  // LED2 on tick, low byte
    static c_uint REG_LED2_ON_H = 0x0F;  // LED2 on tick, high byte
    static c_uint REG_LED2_OFF_L = 0x10; // LED2 off tick, low byte
    static c_uint REG_LED2_OFF_H = 0x11; // LED2 off tick, high byte

    static c_uint REG_LED3_ON_L = 0x12;  // LED3 on tick, low byte
    static c_uint REG_LED3_ON_H = 0x13;  // LED3 on tick, high byte
    static c_uint REG_LED3_OFF_L = 0x14; // LED3 off tick, low byte
    static c_uint REG_LED3_OFF_H = 0x15; // LED3 off tick, high byte

    static c_uint REG_LED4_ON_L = 0x16;  // LED4 on tick, low byte
    static c_uint REG_LED4_ON_H = 0x17;  // LED4 on tick, high byte
    static c_uint REG_LED4_OFF_L = 0x18; // LED4 off tick, low byte
    static c_uint REG_LED4_OFF_H = 0x19; // LED4 off tick, high byte

    static c_uint REG_LED5_ON_L = 0x1A;  // LED5 on tick, low byte
    static c_uint REG_LED5_ON_H = 0x1B;  // LED5 on tick, high byte
    static c_uint REG_LED5_OFF_L = 0x1C; // LED5 off tick, low byte
    static c_uint REG_LED5_OFF_H = 0x1D; // LED5 off tick, high byte

    static c_uint REG_LED6_ON_L = 0x1E;  // LED6 on tick, low byte
    static c_uint REG_LED6_ON_H = 0x1F;  // LED6 on tick, high byte
    static c_uint REG_LED6_OFF_L = 0x20; // LED6 off tick, low byte
    static c_uint REG_LED6_OFF_H = 0x21; // LED6 off tick, high byte

    static c_uint REG_LED7_ON_L = 0x22;  // LED7 on tick, low byte
    static c_uint REG_LED7_ON_H = 0x23;  // LED7 on tick, high byte
    static c_uint REG_LED7_OFF_L = 0x24; // LED7 off tick, low byte
    static c_uint REG_LED7_OFF_H = 0x25; // LED7 off tick, high byte

    static c_uint REG_LED8_ON_L = 0x26;  // LED8 on tick, low byte
    static c_uint REG_LED8_ON_H = 0x27;  // LED8 on tick, high byte
    static c_uint REG_LED8_OFF_L = 0x28; // LED8 off tick, low byte
    static c_uint REG_LED8_OFF_H = 0x29; // LED8 off tick, high byte

    static c_uint REG_LED9_ON_L = 0x2A;  // LED9 on tick, low byte
    static c_uint REG_LED9_ON_H = 0x2B;  // LED9 on tick, high byte
    static c_uint REG_LED9_OFF_L = 0x2C; // LED9 off tick, low byte
    static c_uint REG_LED9_OFF_H = 0x2D; // LED9 off tick, high byte

    static c_uint REG_LED10_ON_L = 0x2E;  // LED10 on tick, low byte
    static c_uint REG_LED10_ON_H = 0x2F;  // LED10 on tick, high byte
    static c_uint REG_LED10_OFF_L = 0x30; // LED10 off tick, low byte
    static c_uint REG_LED10_OFF_H = 0x31; // LED10 off tick, high byte

    static c_uint REG_LED11_ON_L = 0x32;  // LED11 on tick, low byte
    static c_uint REG_LED11_ON_H = 0x33;  // LED11 on tick, high byte
    static c_uint REG_LED11_OFF_L = 0x34; // LED11 off tick, low byte
    static c_uint REG_LED11_OFF_H = 0x35; // LED11 off tick, high byte

    static c_uint REG_LED12_ON_L = 0x36;  // LED12 on tick, low byte
    static c_uint REG_LED12_ON_H = 0x37;  // LED12 on tick, high byte
    static c_uint REG_LED12_OFF_L = 0x38; // LED12 off tick, low byte
    static c_uint REG_LED12_OFF_H = 0x39; // LED12 off tick, high byte

    static c_uint REG_LED13_ON_L = 0x3A;  // LED13 on tick, low byte
    static c_uint REG_LED13_ON_H = 0x3B;  // LED13 on tick, high byte
    static c_uint REG_LED13_OFF_L = 0x3C; // LED13 off tick, low byte
    static c_uint REG_LED13_OFF_H = 0x3D; // LED13 off tick, high byte

    static c_uint REG_LED14_ON_L = 0x3E;  // LED14 on tick, low byte
    static c_uint REG_LED14_ON_H = 0x3F;  // LED14 on tick, high byte
    static c_uint REG_LED14_OFF_L = 0x40; // LED14 off tick, low byte
    static c_uint REG_LED14_OFF_H = 0x41; // LED14 off tick, high byte

    static c_uint REG_LED15_ON_L = 0x42;  // LED15 on tick, low byte
    static c_uint REG_LED15_ON_H = 0x43;  // LED15 on tick, high byte
    static c_uint REG_LED15_OFF_L = 0x44; // LED15 off tick, low byte
    static c_uint REG_LED15_OFF_H = 0x45; // LED15 off tick, high byte

    static c_uint REG_ALLLED_ON_L = 0xFA;  // load all the LEDn_ON registers, low
    static c_uint REG_ALLLED_ON_H = 0xFB;  // load all the LEDn_ON registers, high
    static c_uint REG_ALLLED_OFF_L = 0xFC; // load all the LEDn_OFF registers, low
    static c_uint REG_ALLLED_OFF_H = 0xFD; // load all the LEDn_OFF registers,high
    static c_uint REG_PRESCALE = 0xFE;     // Prescaler for PWM output frequency
    static c_uint REG_TESTMODE = 0xFF;     // defines the test mode to be entered
#pragma endregion

#pragma region /** MPL3115A2 MODE1 register bits **/
    static c_uint REG_MODE1_BIT_ALLCAL = 0x01;  // respond to LED All Call I2C-bus address */
    static c_uint REG_MODE1_BIT_SUB3 = 0x02;    // respond to I2C-bus subaddress 3 */
    static c_uint REG_MODE1_BIT_SUB2 = 0x04;    // respond to I2C-bus subaddress 2 */
    static c_uint REG_MODE1_BIT_SUB1 = 0x08;    // respond to I2C-bus subaddress 1 */
    static c_uint REG_MODE1_BIT_SLEEP = 0x10;   // Low power mode. Oscillator off */
    static c_uint REG_MODE1_BIT_AI = 0x20;      // Auto-Increment enabled */
    static c_uint REG_MODE1_BIT_EXTCLK = 0x40;  // Use EXTCLK pin clock */
    static c_uint REG_MODE1_BIT_RESTART = 0x80; // Restart enabled */
#pragma endregion

#pragma region /** MPL3115A2 MODE2 register bits **/
    static c_uint REG_MODE2_BIT_OUTNE_0 = 0x01; // Active LOW output enable input
    static c_uint REG_MODE2_BIT_OUTNE_1 = 0x02; // Active LOW output enable input - high impedience
    static c_uint REG_MODE2_BIT_OUTDRV = 0x04;  // totem pole structure vs open-drain
    static c_uint REG_MODE2_BIT_OCH = 0x08;     // Outputs change on ACK vs STOP
    static c_uint REG_MODE2_BIT_INVRT = 0x10;   // Output logic state inverted
    // Reserved - 0b0010'0000 - 0x20
    // Reserved - 0b0100'0000 - 0x40
    // Reserved - 0b1000'0000 - 0x80
#pragma endregion
   
/*!
 *  @brief  Class that stores state and functions for interacting with PCA9685
 *          PWM chip
 */
class PCA9685 : I2CDevice {
public:
    PCA9685(unsigned int addr);
    int begin(bool initGpio = true);
    int end(bool terminateGpio = true);

    void reset();
    void sleep();
    void wakeup();

    void setExtClk(unsigned int prescale);
    void setPWMFreq(float freq);

    void setOutputMode(bool totempole);

    unsigned int readPrescale(void);

    unsigned int getPWM(unsigned int num);
    void setPWM(unsigned int num, unsigned int on, unsigned int off);
    void setPPM(unsigned int num, float duty);
    void setServoPPM(unsigned int num, float duty);
};

} // namespace PCA9685

#endif
