#include <algorithm>
#include <iostream>

#include "FlightDriver.h"
#include "./../Util/I2CDevice.h"

namespace FlightDriver {

/*!
 *  @brief  Instantiates a new FlightDriver class
 *  @param  addr
 *          The 7-bit I2C address to locate this chip, default is 0x08
*/
FlightDriver::FlightDriver(unsigned int addr) : I2CDevice(addr) {}

/*!
 *  @brief  Setup the connection to the arduino
 *  @param  initGpio
 *          True if gpioInitialise should be called
 *  @return False on successful setup, error code otherwise
 */
int FlightDriver::begin(bool initGpio) {
    // Init the i2c device
    int begin_response = I2CDevice::begin(initGpio);
    // Check if an error occurred aka response < 0
    if (begin_response < 0) { return begin_response; }
    
    writeWord(0x0, 0x0);
    writeWord(0x1, 0x0);
    writeWord(0x2, 0x0);
    writeWord(0x3, 0x0);

    return 0;
}

/*!
 *  @brief  Set motorspeed to 0 and close all connections
 *  @param  terminateGpio
 *          True if gpioTerminate should be called
 *  @return False on successful setup, error code otherwise
 */
int FlightDriver::end(bool terminateGpio) {
    writeWord(0x0, 0x0);
    writeWord(0x1, 0x0);
    writeWord(0x2, 0x0);
    writeWord(0x3, 0x0);

    // End the i2c device
    return I2CDevice::end(terminateGpio);
}

/*!
 *  @brief  
 *  @return False on successful setup, error code otherwise
 */
int FlightDriver::setDuty(unsigned int motor, float duty) {
    if (duty == 0.0f)
    {
        return  writeWord(motor, 0x0);
    }
    else {
        unsigned int value = (unsigned int) 2000 * (duty / 100);
        if (value < 0)
        {
            value = 0;
        }
        else if (value > 2000)
        {
            value = 2000;
        }
        
        value += 48;
        return writeWord(motor, value);
    } 
}

/*!
 *  @brief  
 *  @return False on successful setup, error code otherwise
 */
int FlightDriver::setConfig(unsigned int motor, unsigned int flag) {
    if (flag > 47) { return -1; }
    writeWord(motor, flag);

    return 0;
}

} // namespace FlightDriver
