#ifndef FLIGHTDRIVER_H
#define FLIGHTDRIVER_H

#include "../Util/I2CDevice.h"

typedef const unsigned int c_uint;

namespace FlightDriver {

static c_uint I2C_ADDRESS = 0x08;
/*!
 *  @brief  
 */
class FlightDriver : I2CDevice {
public:
    FlightDriver(unsigned int addr);
    int begin(bool initGpio = true);
    int end(bool terminateGpio = true);

    int setDuty(unsigned int motor, float duty);
    int setConfig(unsigned int motor, unsigned int flag);
};

} // namespace FlightDriver

#endif // FLIGHTDRIVER_H
