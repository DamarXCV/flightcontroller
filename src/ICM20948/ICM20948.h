
#ifndef _ICM20948_H
#define _ICM20948_H

#include <string.h>
#include <time.h>
#include "../Util/I2CDevice.h"
#include "../Util/Sensor.h"

typedef const unsigned int c_uint;

namespace ICM20948 {

// Misc configuration macros
static c_uint MAG_ID = 0x09;          // The chip ID for the magnetometer

static const float UT_PER_LSB = 0.15; // mag data LSB value (fixed)

static c_uint I2C_MASTER_RESETS_BEFORE_FAIL = 5;// The number of times to try resetting a stuck I2C master before giving up
static c_uint NUM_FINISHED_CHECKS = 100;        // How many times to poll I2C_SLV4_DONE before giving up and resetting

static c_uint I2C_ADDRESS = 0x69;       // ICM20948 default i2c address
static c_uint WHO_AM_I_RESPONSE = 0xEA; // ICM20948 default device id from WHOAMI

#pragma region /** ICM20948 Bank 0 registers **/
    static c_uint B0_REG_WHO_AM_I = 0x00;       // Chip ID register
    // 0x01 -> 0x02 not defined
    static c_uint B0_REG_USER_CTRL = 0x03;      // User Control Reg. Includes I2C Master
    // 0x04 not defined
    static c_uint B0_REG_LP_CONFIG = 0x05;      // Low Power config
    static c_uint B0_REG_PWR_MGMT_1 = 0x06;     // Power management 1
    static c_uint B0_REG_PWR_MGMT_2 = 0x07;     // Power management 2
    // 0x08 -> 0x0E not defined
    static c_uint B0_REG_INT_PIN_CFG = 0xF;     // Interrupt config register
    static c_uint B0_REG_INT_ENABLE = 0x10;     // Interrupt enable register 0
    static c_uint B0_REG_INT_ENABLE_1 = 0x11;   // Interrupt enable register 1
    static c_uint B0_REG_INT_ENABLE_2 = 0x12;   // Interrupt enable register 2
    static c_uint B0_REG_INT_ENABLE_3 = 0x13;   // Interrupt enable register 3
    // 0x14 -> 0x16 not defined
    static c_uint B0_REG_I2C_MST_STATUS = 0x17; // Records if I2C master bus data is finished
    // 0x18 not defined
    static c_uint B0_REG_INT_STATUS = 0x19;
    static c_uint B0_REG_INT_STATUS_1 = 0x1A;
    static c_uint B0_REG_INT_STATUS_2 = 0x1B;
    static c_uint B0_REG_INT_STATUS_3 = 0x1C;
    // 0x1D -> 0x27 not defined
    static c_uint B0_REG_DELAY_TIMEH = 0x28;
    static c_uint B0_REG_DELAY_TIMEL = 0x29;
    // 0x2A -> 0x2C not defined
    static c_uint B0_REG_ACCEL_XOUT_H = 0x2D;   // first byte of accel data
    static c_uint B0_REG_ACCEL_XOUT_L = 0x2E;
    static c_uint B0_REG_ACCEL_YOUT_H = 0x2F;
    static c_uint B0_REG_ACCEL_YOUT_L = 0x30;
    static c_uint B0_REG_ACCEL_ZOUT_H = 0x31;
    static c_uint B0_REG_ACCEL_ZOUT_L = 0x32;
    static c_uint B0_REG_GYRO_XOUT_H = 0x33;    // first byte of gyro data
    static c_uint B0_REG_GYRO_XOUT_L = 0x34;
    static c_uint B0_REG_GYRO_YOUT_H = 0x35;
    static c_uint B0_REG_GYRO_YOUT_L = 0x36;
    static c_uint B0_REG_GYRO_ZOUT_H = 0x37;
    static c_uint B0_REG_GYRO_ZOUT_L = 0x38;
    static c_uint B0_REG_TEMP_OUT_H = 0x39;     // first byte of temperature data
    static c_uint B0_REG_TEMP_OUT_L = 0x3A;

    static c_uint B0_REG_EXT_SLV_SENS_DATA_00 = 0x3B;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_01 = 0x3C;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_02 = 0x3D;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_03 = 0x3E;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_04 = 0x3F;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_05 = 0x40;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_06 = 0x41;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_07 = 0x42;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_08 = 0x43;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_09 = 0x44;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_10 = 0x45;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_11 = 0x46;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_12 = 0x47;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_13 = 0x48;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_14 = 0x49;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_15 = 0x4A;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_16 = 0x4B;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_17 = 0x4C;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_18 = 0x4D;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_19 = 0x4E;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_20 = 0x4F;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_21 = 0x50;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_22 = 0x51;
    static c_uint B0_REG_EXT_SLV_SENS_DATA_23 = 0x52;
    // 0x53 -> 0x65 not defined
    static c_uint B0_REG_FIFO_EN_1 = 0x66;
    static c_uint B0_REG_FIFO_EN_2 = 0x67;
    static c_uint B0_REG_FIFO_RST = 0x68;
    static c_uint B0_REG_FIFO_MODE = 0x69;
    static c_uint B0_REG_FIFO_COUNTH = 0x70;
    static c_uint B0_REG_FIFO_COUNTL = 0x71;
    static c_uint B0_REG_FIFO_R_W = 0x72;
    // 0x73 not defined
    static c_uint B0_REG_DATA_RDY_STATUS = 0x74;
    static c_uint B0_REG_FIFO_CFG = 0x76;
    // 0x77 -> 0x7E not defined
    static c_uint B0_REG_REG_BANK_SEL = 0x7F;   // register bank selection register
#pragma endregion

#pragma region /** ICM20948 Bank 1 registers **/
    // 0x01 not defined
    static c_uint B1_REG_SELF_TEST_X_GYRO = 0x02;
    static c_uint B1_REG_SELF_TEST_Y_GYRO = 0x03;
    static c_uint B1_REG_SELF_TEST_Z_GYRO = 0x04;
    // 0x05 -> 0x0D not defined
    static c_uint B1_REG_SELF_TEST_X_ACCEL = 0x0E;
    static c_uint B1_REG_SELF_TEST_Y_ACCEL = 0x0F;
    static c_uint B1_REG_SELF_TEST_Z_ACCEL = 0x10;
    // 0x11 -> 0x13 not defined
    static c_uint B1_REG_XA_OFFS_H = 0x14;
    static c_uint B1_REG_XA_OFFS_L = 0x15;
    // 0x16 not defined
    static c_uint B1_REG_YA_OFFS_H = 0x17;
    static c_uint B1_REG_YA_OFFS_L = 0x18;
    // 0x19 not defined
    static c_uint B1_REG_ZA_OFFS_H = 0x1A;
    static c_uint B1_REG_ZA_OFFS_L = 0x1B;
    // 0x1C -> 0x27 not defined
    static c_uint B1_REG_TIMEBASE_CORRECTION_PLL = 0x28;
    // 0291 -> 0x7E not defined
    static c_uint B1_REG_REG_BANK_SEL = 0x7F;
#pragma endregion

#pragma region /** ICM20948 Bank 2 registers **/
    static c_uint B2_REG_GYRO_SMPLRT_DIV = 0x00;    // Gyroscope data rate divisor
    static c_uint B2_REG_GYRO_CONFIG_1 = 0x01;      // Gyro config for range setting
    static c_uint B2_REG_GYRO_CONFIG_2 = 0x02;      // Gyro config for range setting
    
    static c_uint B2_REG_XG_OFFS_USRH = 0x03;       // register for offset
    static c_uint B2_REG_XG_OFFS_USRL = 0x04;
    static c_uint B2_REG_YG_OFFS_USRH = 0x05;
    static c_uint B2_REG_YG_OFFS_USRL = 0x06;
    static c_uint B2_REG_ZG_OFFS_USRH = 0x07;
    static c_uint B2_REG_ZG_OFFS_USRL = 0x08;
    
    static c_uint B2_REG_ODR_ALIGN_EN = 0x09;
    
    static c_uint B2_REG_ACCEL_SMPLRT_DIV_1 = 0x10; // Accel data rate divisor MSByte
    static c_uint B2_REG_ACCEL_SMPLRT_DIV_2 = 0x11; // Accel data rate divisor LSByte
    static c_uint B2_REG_ACCEL_INTEL_CTRL = 0x12;
    static c_uint B2_REG_ACCEL_WOM_THR = 0x13;
    static c_uint B2_REG_ACCEL_CONFIG = 0x14;     // Accel config for setting range
    static c_uint B2_REG_ACCEL_CONFIG_2 = 0x15;
    // 0x16 -> 0x51 not defined
    static c_uint B2_REG_FSYNC_CONFIG = 0x52;
    static c_uint B2_REG_TEMP_CONFIG = 0x53;
    static c_uint B2_REG_MOD_CTRL_USR = 0x54;
    // 0x55 -> 0x7E not defined
    static c_uint B2_REG_REG_BANK_SEL = 0x7F;
#pragma endregion

#pragma region /** ICM20948 Bank 3 registers **/
    static c_uint B3_REG_I2C_MST_ODR_CONFIG = 0x00; // Sets ODR for I2C master bus
    static c_uint B3_REG_I2C_MST_CTRL = 0x01;       // I2C master bus config
    static c_uint B3_REG_I2C_MST_DELAY_CTRL = 0x02; // I2C master bus config

    static c_uint B3_REG_I2C_SLV0_ADDR = 0x03;      // Sets I2C address for I2C master bus slave 0
    static c_uint B3_REG_I2C_SLV0_REG = 0x04;       // Sets register address for I2C master bus slave 0
    static c_uint B3_REG_I2C_SLV0_CTRL = 0x05;      // Controls for I2C master bus slave 0
    static c_uint B3_REG_I2C_SLV0_DO = 0x06;        // Sets I2C master bus slave 0 data out

    static c_uint B3_REG_I2C_SLV1_ADDR = 0x07;      // Sets I2C address for I2C master bus slave 1
    static c_uint B3_REG_I2C_SLV1_REG = 0x08;       // Sets register address for I2C master bus slave 
    static c_uint B3_REG_I2C_SLV1_CTRL = 0x09;      // Controls for I2C master bus slave 1
    static c_uint B3_REG_I2C_SLV1_DO = 0x0A;        // Sets I2C master bus slave 1 data out

    static c_uint B3_REG_I2C_SLV2_ADDR = 0x0B;      // Sets I2C address for I2C master bus slave 2
    static c_uint B3_REG_I2C_SLV2_REG = 0x0C;       // Sets register address for I2C master bus slave 2
    static c_uint B3_REG_I2C_SLV2_CTRL = 0x0D;      // Controls for I2C master bus slave 2
    static c_uint B3_REG_I2C_SLV2_DO = 0x0E;        // Sets I2C master bus slave 2 data out

    static c_uint B3_REG_I2C_SLV3_ADDR = 0x0F;      // Sets I2C address for I2C master bus slave 3
    static c_uint B3_REG_I2C_SLV3_REG = 0x10;       // Sets register address for I2C master bus slave 3
    static c_uint B3_REG_I2C_SLV3_CTRL = 0x11;      // Controls for I2C master bus slave 3
    static c_uint B3_REG_I2C_SLV3_DO = 0x012;       // Sets I2C master bus slave 3 data out

    static c_uint B3_REG_I2C_SLV4_ADDR = 0x13;      // Sets I2C address for I2C master bus slave 4
    static c_uint B3_REG_I2C_SLV4_REG =  0x14;      // Sets register address for I2C master bus slave 4
    static c_uint B3_REG_I2C_SLV4_CTRL = 0x15;      // Controls for I2C master bus slave 4
    static c_uint B3_REG_I2C_SLV4_DO = 0x16;        // Sets I2C master bus slave 4 data out
    static c_uint B3_REG_I2C_SLV4_DI = 0x17;        // Sets I2C master bus slave 4 data in
    
    static c_uint B3_REG_REG_BANK_SEL = 0x7F;
#pragma endregion

#pragma region  /** Magnetometer values **/
    static c_uint AK09916_WIA2 = 0x01;
    static c_uint AK09916_ST1  = 0x10;
    static c_uint AK09916_HXL  = 0x11;
    static c_uint AK09916_HXH  = 0x12;
    static c_uint AK09916_HYL  = 0x13;
    static c_uint AK09916_HYH  = 0x14;
    static c_uint AK09916_HZL  = 0x15;
    static c_uint AK09916_HZH  = 0x16;
    static c_uint AK09916_ST2  = 0x18;
    static c_uint AK09916_CNTL2= 0x31;
    static c_uint AK09916_CNTL3= 0x32;
#pragma endregion


/** Options for `enableAccelDLPF` */
typedef enum {
    ACCEL_FREQ_246_0_HZ = 0x01,
    ACCEL_FREQ_111_4_HZ = 0x02,
    ACCEL_FREQ_50_4_HZ = 0x03,
    ACCEL_FREQ_23_9_HZ = 0x04,
    ACCEL_FREQ_11_5_HZ = 0x05,
    ACCEL_FREQ_5_7_HZ = 0x06,
    ACCEL_FREQ_473_HZ = 0x07,
} accel_cutoff_t;

/** Options for `enableGyroDLPF` */
typedef enum {
    GYRO_FREQ_196_6_HZ = 0x00,
    GYRO_FREQ_151_8_HZ = 0x01,
    GYRO_FREQ_119_5_HZ = 0x02,
    GYRO_FREQ_51_2_HZ = 0x03,
    GYRO_FREQ_23_9_HZ = 0x04,
    GYRO_FREQ_11_6_HZ = 0x05,
    GYRO_FREQ_5_7_HZ = 0x06,
    GYRO_FREQ_361_4_HZ = 0x07,
} gyro_cutoff_t;

/** The accelerometer data range */
typedef enum {
    ACCEL_RANGE_2_G,
    ACCEL_RANGE_4_G,
    ACCEL_RANGE_8_G,
    ACCEL_RANGE_16_G,
} accel_range_t;

/** The gyro data range */
typedef enum {
    GYRO_RANGE_250_DPS,
    GYRO_RANGE_500_DPS,
    GYRO_RANGE_1000_DPS,
    GYRO_RANGE_2000_DPS,
} gyro_range_t;

/**
 * @brief Data rates/modes for the embedded AsahiKASEI AK09916 3-axis
 * magnetometer
 */
typedef enum {
    AK09916_MAG_DATARATE_SHUTDOWN = 0x0, // Stops measurement updates
    AK09916_MAG_DATARATE_SINGLE = 0x1, // Takes a single measurement
                    // then switches to AK09916_MAG_DATARATE_SHUTDOWN
    AK09916_MAG_DATARATE_10_HZ = 0x2,  // updates at 10Hz
    AK09916_MAG_DATARATE_20_HZ = 0x4,  // updates at 20Hz
    AK09916_MAG_DATARATE_50_HZ = 0x6,  // updates at 50Hz
    AK09916_MAG_DATARATE_100_HZ = 0x8, // updates at 100Hz
} ak09916_data_rate_t;

class ICM20948;

/* Sensor interface for accelerometer component of ICM20948 */
class ICM20948_Accelerometer : I2CDevice {
public:
    ICM20948_Accelerometer(ICM20948 *_theICM20948);
    void getEvent(unsigned int t, sensors_event_t *);
    void getSensor(sensor_t *);

private:
    int _sensorID = 0x20A;
    ICM20948 *_theICM20948 = nullptr;
};

/* Sensor interface for gyro component of ICM20948 */
class ICM20948_Gyro : public I2CDevice {
public:
    ICM20948_Gyro(ICM20948 *_theICM20948);
    void getEvent(unsigned int t, sensors_event_t *);
    void getSensor(sensor_t *);

private:
    int _sensorID = 0x20B;
    ICM20948 *_theICM20948 = nullptr;
};

/* Sensor interface for magnetometer component of ICM20948 */
class ICM20948_Magnetometer : public I2CDevice {
public:
    ICM20948_Magnetometer(ICM20948 *_theICM20948);
    void getEvent(unsigned int t, sensors_event_t *);
    void getSensor(sensor_t *);

private:
    int _sensorID = 0x20C;
    ICM20948 *_theICM20948 = nullptr;
};

/*!
 *    @brief  Class that stores state and functions for interacting with
 *            the ST ICM2948 9-DoF Accelerometer, gyro, and magnetometer
 */
class ICM20948 : public I2CDevice {
public:

    ICM20948(unsigned int addr = I2C_ADDRESS);
    ~ICM20948(void){
        if (accel_sensor)
            delete accel_sensor;
        if (gyro_sensor)
            delete gyro_sensor;
        if (mag_sensor)
            delete mag_sensor;
    }

    ICM20948_Accelerometer *getAccelerometerSensor(void);
    ICM20948_Gyro *getGyroSensor(void);
    ICM20948_Magnetometer *getMagnetometerSensor(void);

    int begin(unsigned int sensor_id = 0, bool initGpio = true);
    int end(bool terminateGpio = true);

    void reset(void);

    int setBank(unsigned int bank);

    accel_range_t getAccelRange(void);
    void setAccelRange(accel_range_t new_accel_range);

    gyro_range_t getGyroRange(void);
    void setGyroRange(gyro_range_t new_gyro_range);

    ak09916_data_rate_t getMagDataRate(void);
    bool setMagDataRate(ak09916_data_rate_t rate);

    unsigned int getGyroRateDivisor(void);
    void setGyroRateDivisor(unsigned int new_gyro_divisor);

    unsigned int getAccelRateDivisor(void);
    void setAccelRateDivisor(unsigned int new_accel_divisor);

    bool enableAccelDLPF(bool enable, accel_cutoff_t cutoff_freq);
    bool enableGyrolDLPF(bool enable, gyro_cutoff_t cutoff_freq);

    void setInt1ActiveLow(bool active_low);
    void setInt2ActiveLow(bool active_low);

    bool getEvent(unsigned int t, sensors_event_t *accel, sensors_event_t *gyro,
                    sensors_event_t *mag = nullptr);

    unsigned int readExternalRegister(unsigned int slv_addr, unsigned int reg_addr);
    bool writeExternalRegister(unsigned int slv_addr, unsigned int reg_addr, unsigned int value);
    bool configureI2CMaster(void);
    bool enableI2CMaster(bool enable_i2c_master);
    void resetI2CMaster(void);
    void setI2CBypass(bool bypass_i2c);

protected:
    ICM20948_Accelerometer *accel_sensor = nullptr; // Accelerometer data object
    ICM20948_Gyro *gyro_sensor = nullptr;           // Gyro data object
    ICM20948_Magnetometer *mag_sensor = nullptr;    // Magnetometer sensor data object

    float accX,     // Last reading's accelerometer X axis m/s²
        accY,       // Last reading's accelerometer Y axis m/s²
        accZ,       // Last reading's accelerometer Z axis m/s²
        gyroX,      // Last reading's gyro X axis in rad/s
        gyroY,      // Last reading's gyro Y axis in rad/s
        gyroZ,      // Last reading's gyro Z axis in rad/s
        magX,       // Last reading's mag X axis in uT
        magY,       // Last reading's mag Y axis in uT
        magZ;       // Last reading's mag Z axis in uT

    short rawAccX,  // temp variables
        rawAccY,    // temp variables
        rawAccZ,    // temp variables
        rawGyroX,   // temp variables
        rawGyroY,   // temp variables
        rawGyroZ,   // temp variables
        rawMagX,    // temp variables
        rawMagY,    // temp variables
        rawMagZ;    // temp variables

    unsigned int _sensorid_accel,   // ID number for accelerometer
                _sensorid_gyro,     // ID number for gyrometer
                _sensorid_mag;      // ID number for magnetometer

    unsigned int current_accel_range; // accelerometer range cache
    unsigned int current_gyro_range;  // gyro range cache

    void _read(void);
    bool _init(unsigned int sensor_id);

    unsigned int readAccelRange(void);
    void writeAccelRange(unsigned int new_accel_range);

    unsigned int readGyroRange(void);
    void writeGyroRange(unsigned int new_gyro_range);

private:
    friend class ICM20948_Accelerometer;// Gives access to private members to Accelerometer data object
    friend class ICM20948_Gyro;         // Gives access to private members to Gyro data object
    friend class ICM20948_Magnetometer; // Gives access to private members to Magnetometer data object

    unsigned int readMagRegister(unsigned int reg_addr);
    bool writeMagRegister(unsigned int reg_addr, unsigned int value);

    unsigned int getMagId(void);
    bool auxI2CBusSetupFailed(void);

    bool setupMag(void);
    void scaleValues(void);
                                                
    void fillAccelEvent(sensors_event_t *accel, unsigned int timestamp);
    void fillGyroEvent(sensors_event_t *gyro, unsigned int timestamp);
    void fillMagEvent(sensors_event_t *mag, unsigned int timestamp);
    unsigned int auxillaryRegisterTransaction(bool read, unsigned int slv_addr,
                                        unsigned int reg_addr, unsigned int value = 1);
};

} // namespace ICM20948

#endif
