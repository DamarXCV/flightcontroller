/*!   @file ICM20948.cpp
 */
#include "ICM20948.h"
#include <iostream>
#include "../Util/Helper.h"

namespace ICM20948 {

/*!
 *  @brief  Instantiates a new ICM20948 class
 */
ICM20948::ICM20948(unsigned int addr) : I2CDevice(addr) {}

/*!
 *  @brief  set a bank on an ICM20948
 *  @param  bank
 *          the bank to set
 *  @return 0 if success else error code
 */
int ICM20948::setBank(unsigned int bank) {
    return write(B0_REG_REG_BANK_SEL, (bank & 0b11) << 4);
}

/*!
 *  @brief  begin I²C connunication on an ICM20948
 *  @param  sensor_id
 *          the bank to set
 *  @param  initGpio
 *          True if gpioInitialise should be called
 *  @return 1 if success else 0 if failed to init or error code if negative or device id if greater than 1
 */
int ICM20948::begin(unsigned int sensor_id, bool initGpio) {
    // init the i2c device
    int begin_response = I2CDevice::begin(initGpio);
    // Check if an error occurred - if - then return error code
    if (begin_response < 0) { return begin_response; }
    
    // Check if the device is working
    int who_am_i_response = read(B0_REG_WHO_AM_I);
    // check if response is correct else return response
    if (who_am_i_response != WHO_AM_I_RESPONSE) { return who_am_i_response; }
    // try to init sensor
    bool init_success = ICM20948::_init(sensor_id);
    if (init_success && !setupMag()) {
        std::cout << "failed to setup mag";
        return false;
    }

    return init_success;
}

/*!
 *  @brief  Close all connections
 *  @param  terminateGpio
 *          True if gpioTerminate should be called
 *  @return False on successful setup, error code otherwise
 */
int ICM20948::end(bool terminateGpio) {
    // End the i2c device
    return I2CDevice::end(terminateGpio);
}

/*!
 *  @brief  Reset the internal registers and restores the default settings
 */
void ICM20948::reset(void) {
    setBank(0);
    // write reset bit
    writeBits(B0_REG_PWR_MGMT_1, 1, 1, 7);
    Util::sleep(20);
    // check if reset is not done
    while (readBits(B0_REG_PWR_MGMT_1, 1, 7)) {
        Util::sleep(10);
    };
    Util::sleep(50);
}

/*!
 *  @brief  Initilizes the sensor
 *  @param  sensor_id
 *          Optional unique ID for the sensor set
 *  @return True if chip identified and initialized, false otherwise
 */

bool ICM20948::_init(unsigned int sensor_id) {
    setBank(0);

    _sensorid_accel = sensor_id;
    _sensorid_gyro = sensor_id + 1;
    _sensorid_mag = sensor_id + 2;

    reset();

    writeBits(B0_REG_PWR_MGMT_1, false, 1, 6);

    // 3 will be the largest range for either sensor
    writeGyroRange(3);
    writeAccelRange(3);

    // 1100Hz/(1+10) = 100Hz
    setGyroRateDivisor(10);

    // # 1125Hz/(1+10) = 102.27Hz
    setAccelRateDivisor(10);

    accel_sensor = new ICM20948_Accelerometer(this);
    gyro_sensor = new ICM20948_Gyro(this);
    mag_sensor = new ICM20948_Magnetometer(this);
    Util::sleep(20);

    return true;
}

/*!
 *  @brief  Gets the most recent sensor event
 *  @param  t
 *          time since program start in ms.
 *  @param  accel
 *          Pointer to an sensor_event_t object to be filled
 *          with acceleration event data.
 *  @param  gyro
 *          Pointer to an sensor_event_t object to be filled
 *          with gyro event data.
 *  @param  mag
 *          Pointer to an sensor_event_t object to be filled
 *          with magnetometer event data.
 *  @return True on successful read
 */
bool ICM20948::getEvent(unsigned int t, sensors_event_t *accel, sensors_event_t *gyro,
                                                                sensors_event_t *mag) {
    _read();

    // use helpers to fill in the events
    fillAccelEvent(accel, t);
    fillGyroEvent(gyro, t);
    fillMagEvent(mag, t);

    return true;
}

/*!
 *  @brief  Fills the accel object with the most recent sensor event data
 *  @param  accel
 *          Pointer to an sensor_event_t object to be filled
 *          with acceleration event data.
 *  @param  timestamp
 *          time since program start in ms.
 */
void ICM20948::fillAccelEvent(sensors_event_t *accel, unsigned int timestamp) {
    memset(accel, 0, sizeof(sensors_event_t));
    accel->version = 1;
    accel->sensor_id = _sensorid_accel;
    accel->type = SENSOR_TYPE_ACCELEROMETER;
    accel->timestamp = timestamp;

    accel->acceleration.x = accX * SENSORS_GRAVITY_EARTH;   // last reading * gravity
    accel->acceleration.y = accY * SENSORS_GRAVITY_EARTH;
    accel->acceleration.z = accZ * SENSORS_GRAVITY_EARTH;
}

/*!
 *  @brief  Fills the gyro object with the most recent sensor event data
 *  @param  accel
 *          Pointer to an sensor_event_t object to be filled
 *          with gyro event data.
 *  @param  timestamp
 *          time since program start in ms.
 */
void ICM20948::fillGyroEvent(sensors_event_t *gyro, unsigned int timestamp) {
    memset(gyro, 0, sizeof(sensors_event_t));
    gyro->version = 1;
    gyro->sensor_id = _sensorid_gyro;
    gyro->type = SENSOR_TYPE_GYROSCOPE;
    gyro->timestamp = timestamp;

    gyro->gyro.x = gyroX * SENSORS_DPS_TO_RADS;     // last reading * deg-to-rad multiplier
    gyro->gyro.y = gyroY * SENSORS_DPS_TO_RADS;
    gyro->gyro.z = gyroZ * SENSORS_DPS_TO_RADS;
}

/*!
 *  @brief  Fills the mag object with the most recent sensor event data
 *  @param  accel
 *          Pointer to an sensor_event_t object to be filled
 *          with mag event data.
 *  @param  timestamp
 *          time since program start in ms.
 */
void ICM20948::fillMagEvent(sensors_event_t *mag, unsigned int timestamp) {
    memset(mag, 0, sizeof(sensors_event_t));
    mag->version = 1;
    mag->sensor_id = _sensorid_mag;
    mag->type = SENSOR_TYPE_MAGNETIC_FIELD;
    mag->timestamp = timestamp;

    mag->magnetic.x = magX;     // last reading
    mag->magnetic.y = magY;
    mag->magnetic.z = magZ;
}

/******************* Sensor functions *****************/
/*!
 *  @brief  Updates the measurement data for all sensors simultaneously
 */
void ICM20948::_read(void) {

    setBank(0);

    // reading 9 bytes of mag data to fetch the register that tells the mag we've
    // read all the data
    const unsigned int numbytes = 12 + 9; // Read Accel, gyro, and 9 bytes of mag
    
    char buffer[numbytes];
    readBlock(B0_REG_ACCEL_XOUT_H, buffer, numbytes);

    rawAccX = buffer[0] << 8 | buffer[1];       // read accel data
    rawAccY = buffer[2] << 8 | buffer[3];
    rawAccZ = buffer[4] << 8 | buffer[5];

    rawGyroX = buffer[6] << 8 | buffer[7];      // read gyro data
    rawGyroY = buffer[8] << 8 | buffer[9];
    rawGyroZ = buffer[10] << 8 | buffer[11];

    rawMagX = ((buffer[16] << 8) | (buffer[15] & 0xFF)); // read mag data
    rawMagY = ((buffer[18] << 8) | (buffer[17] & 0xFF));
    rawMagZ = ((buffer[20] << 8) | (buffer[19] & 0xFF));

    scaleValues();
    setBank(0);
}

/*!
 *  @brief  Gets an Sensor object for the accelerometer sensor component
 *  @return Sensor pointer to accelerometer sensor
 */
ICM20948_Accelerometer *ICM20948::getAccelerometerSensor(void) {
    return accel_sensor;
}

/*!
 *  @brief  Gets an Sensor object for the gyro sensor component
 *  @return Sensor pointer to gyro sensor
 */
ICM20948_Gyro *ICM20948::getGyroSensor(void) { 
    return gyro_sensor;
}

/*!
 *  @brief  Gets an Sensor object for the magnetometer sensor component
 *  @return Sensor pointer to magnetometer sensor
 */
ICM20948_Magnetometer *ICM20948::getMagnetometerSensor(void) {
    return mag_sensor;
}

/*!
 *  @brief  Get the accelerometer's measurement range.
 *  @return The accelerometer's measurement range.
 */

unsigned int ICM20948::readAccelRange(void) {
    setBank(2);

    unsigned int range = readBits(B2_REG_ACCEL_CONFIG, 2, 1);

    setBank(0);
    return range;
}

/*!
 *  @brief  Sets the accelerometer's measurement range.
 *  @param  new_accel_range
 *          Measurement range to be set. Must be an `ICM20948_accel_range_t`.
 */
void ICM20948::writeAccelRange(unsigned int new_accel_range) {
    setBank(2);

    writeBits(B2_REG_ACCEL_CONFIG, new_accel_range, 2, 1);

    current_accel_range = new_accel_range;

    setBank(0);
}

/*!
 *  @brief  Get the gyro's measurement range.
 *  @return The gyro's measurement range.
 */
unsigned int ICM20948::readGyroRange(void) {
    setBank(2);

    unsigned int range = readBits(B2_REG_GYRO_CONFIG_1, 2, 1);

    setBank(0);
    return range;
}

/*!
 *  @brief  Sets the gyro's measurement range.
 *  @param  new_gyro_range
 *          Measurement range to be set. Must be an `ICM20948_gyro_range_t`.
 */
void ICM20948::writeGyroRange(unsigned int new_gyro_range) {
    setBank(2);

    writeBits(B2_REG_GYRO_CONFIG_1, new_gyro_range, 2, 1);

    current_gyro_range = new_gyro_range;
    setBank(0);
}

/*!
 *  @brief  Get the accelerometer's data rate divisor.
 *  @return The accelerometer's data rate divisor.
 */
unsigned int ICM20948::getAccelRateDivisor(void) {
    setBank(2);

    char* buffer = new char[2];
    
    readBlock(B2_REG_ACCEL_SMPLRT_DIV_1, buffer, 2);

    unsigned int divisor_val = buffer[0] << 8 | buffer[1];

    delete[] buffer;

    setBank(0);
    return divisor_val;
}

/*!
 *  @brief  Sets the accelerometer's data rate divisor.
 *  @param  new_accel_divisor
 *          The accelerometer's data rate divisor (12 bit)
 */
void ICM20948::setAccelRateDivisor(unsigned int new_accel_divisor) {
    setBank(2);

    char* buffer = new char[2];

    buffer[1] = new_accel_divisor & 0xFF;
    new_accel_divisor >>= 8;
    buffer[0] = new_accel_divisor & 0xFF;

    writeBlock(B2_REG_ACCEL_SMPLRT_DIV_1, buffer, 2);

    setBank(0);
}

/*!
 *  @brief  Get the gyro's data rate divisor.
 *  @return The gyro's data rate divisor.
 */
unsigned int ICM20948::getGyroRateDivisor(void) {
    setBank(2);

    unsigned int divisor_val = read(B2_REG_GYRO_SMPLRT_DIV);

    setBank(0);
    return divisor_val;
}

/*!
 *  @brief  Sets the gyro's data rate divisor.
 *  @param  new_gyro_divisor
 *          The gyro's data rate divisor.
 */
void ICM20948::setGyroRateDivisor(unsigned int new_gyro_divisor) {
    setBank(2);

    write(B2_REG_GYRO_SMPLRT_DIV, new_gyro_divisor);

    setBank(0);
}

/*!
 *  @brief  Enable or disable the accelerometer's Digital Low Pass Filter
 *  @param  enable
 *  @param  cutoff_freq
 *          Signals changing at a rate higher than the given cutoff
 *          frequency will be filtered out
 *  @return true: success false: failure
 */
bool ICM20948::enableAccelDLPF(bool enable, accel_cutoff_t cutoff_freq) {
    setBank(2);

    if (!writeBits(B2_REG_ACCEL_CONFIG, enable, 1, 0))
        return false;

    if (!enable) 
        return true;

    if (!writeBits(B2_REG_ACCEL_CONFIG, cutoff_freq, 3, 3))
        return false;

    return true;
}

/*!
 *  @brief  Enable or disable the gyro's Digital Low Pass Filter
 *  @param  enable
 *          true: enable false: disable
 *  @param  cutoff_freq
 *          Signals changing at a rate higher than the given cutoff
 *          frequency will be filtered out
 *  @return true: success false: failure
 */
bool ICM20948::enableGyrolDLPF(bool enable, gyro_cutoff_t cutoff_freq) {
    setBank(2);

    if(!writeBits(B2_REG_ACCEL_CONFIG, enable, 1, 0))
        return false;

    if (!enable)
        return true;

    if (!writeBits(B2_REG_ACCEL_CONFIG, cutoff_freq, 3, 3))
        return false;

    return true;
}

/*!
 *  @brief  Sets the polarity of the int1 pin
 *  @param  active_low
 *          Set to true to make INT1 active low, false to make it active high
 */
void ICM20948::setInt1ActiveLow(bool active_low) {
    setBank(0);

    writeBits(B0_REG_INT_PIN_CFG, true, 1, 6);
    writeBits(B0_REG_INT_PIN_CFG, active_low, 1, 7);
}

/*!
 *  @brief  Sets the polarity of the INT2 pin
 *  @param  active_low
 *          Set to true to make INT1 active low, false to make it active high
 */
void ICM20948::setInt2ActiveLow(bool active_low) {
    setBank(0);

    writeBits(B0_REG_INT_ENABLE_1, true, 1, 6);
    writeBits(B0_REG_INT_ENABLE_1, active_low, 1, 7);
}

/*!
 *  @brief  Sets the bypass status of the I2C master bus support.
 *  @param  bypass_i2c
 *          Set to true to bypass the internal I2C master circuitry, connecting
 *          the external I2C bus to the main I2C bus. Set to false to re-connect
 */
void ICM20948::setI2CBypass(bool bypass_i2c) {
    setBank(0);

    writeBits(B0_REG_INT_PIN_CFG, (unsigned int) bypass_i2c, 1, 1);
}

/*!
 *  @brief  Enable or disable the I2C mastercontroller
 *  @param  enable_i2c_master
 *          true: enable false: disable
 *  @return true: success false: error
 */

bool ICM20948::enableI2CMaster(bool enable_i2c_master) {
    setBank(0);

    return writeBits(B0_REG_USER_CTRL, enable_i2c_master, 1, 5);
}

/*!
 *  @brief  Set the I2C clock rate for the auxillary I2C bus to 345.60kHz and
 *          disable repeated start
 *  @return true: success false: failure
 */

bool ICM20948::configureI2CMaster(void) {
    setBank(3);

    return write(B3_REG_I2C_MST_CTRL, 0x17);
}

/*!
 *  @brief  Read a single byte from a given register address for an I2C slave
 *          device on the auxiliary I2C bus
 *  @param  slv_addr
 *          the 7-bit I2C address of the slave device
 *  @param  reg_addr
 *          the register address to read from
 *  @return the requested register value
 */
unsigned int ICM20948::readExternalRegister(unsigned int slv_addr, unsigned int reg_addr) {
    return auxillaryRegisterTransaction(true, slv_addr, reg_addr);
}

/*!
 *  @brief  Write a single byte to a given register address for an I2C slave
 *          device on the auxiliary I2C bus
 *  @param  slv_addr
 *          the 7-bit I2C address of the slave device
 *  @param  reg_addr
 *          the register address to write to
 *  @param  value
 *          the value to write
 *  @return true: success false: failure
 */
bool ICM20948::writeExternalRegister(unsigned int slv_addr, unsigned int reg_addr,
                                                            unsigned int value) {

  return (bool)auxillaryRegisterTransaction(false, slv_addr, reg_addr, value);
}

/*!
 *  @brief  Write a single byte to a given register address for an I2C slave
 *          device on the auxiliary I2C bus
 *  @param  slv_addr
 *          the 7-bit I2C address of the slave device
 *  @param  reg_addr
 *          the register address to write to
 *  @param  value
 *          the value to write
 *  @return true: success false: failure
 */

unsigned int ICM20948::auxillaryRegisterTransaction(bool _read, unsigned int slv_addr,
                                                    unsigned int reg_addr,
                                                    unsigned int value) {
    setBank(3);

    if (_read) {
        slv_addr |= 0x80; // set high bit for read, presumably for multi-byte reads
    } else {
        if (write(B3_REG_I2C_SLV4_DO, value))
            return (unsigned int) false;
    }

    if (write(B3_REG_I2C_SLV4_ADDR, slv_addr))
        return (unsigned int) false;
    
    if (write(B3_REG_I2C_SLV4_REG, reg_addr))
        return (unsigned int) false;

    if (write(B3_REG_I2C_SLV4_CTRL, 0x80))
        return (unsigned int) false;

    setBank(0);
    unsigned int tries = 0;
    // wait until the operation is finished
    while (readBits(B0_REG_I2C_MST_STATUS, 1, 6) != (int) true) {
        tries++;
        if (tries >= NUM_FINISHED_CHECKS)
            return (unsigned int) false;
    }
    if (_read) {
        setBank(3);
        return read(B3_REG_I2C_SLV4_DI);
  }
  return (unsigned int) true;
}

/*!
 *  @brief  Reset the I2C master
 */
void ICM20948::resetI2CMaster(void) {
    setBank(0);

    writeBits(B0_REG_USER_CTRL, 1, 1, 1);
    while (readBits(B0_REG_USER_CTRL, 1, 1)) {
        Util::sleep(10);
    }
    Util::sleep(100);
}

/*!
 *  @brief  Checks if I²C Bus setup fails
 *  @return true: setup fails  false: setup successful
 */
bool ICM20948::auxI2CBusSetupFailed(void) {
    // check aux I2C bus connection by reading the magnetometer chip ID
    bool aux_i2c_setup_failed = true;
    for (int i = 0; i < I2C_MASTER_RESETS_BEFORE_FAIL; i++) {
        if (getMagId() != MAG_ID) {
            resetI2CMaster();
        } else {
            aux_i2c_setup_failed = false;
            break;
        }
    }
    return aux_i2c_setup_failed;
}

/*!
 *  @brief  Checks for the Mag ID
 *  @return The Mag ID
 */
unsigned int ICM20948::getMagId(void) {
  // verify the magnetometer id
  return readExternalRegister(0x8C, 0x01);
}

/*!
 *  @brief  Setup for the Mag
 *  @return true: sucessful  false: failed
 */
bool ICM20948::setupMag(void) {
    unsigned int buffer[2];

    setI2CBypass(false);

    configureI2CMaster();

    enableI2CMaster(true);

    if (auxI2CBusSetupFailed()) {
        return false;
    }
    // set mag data rate
    if (!setMagDataRate(AK09916_MAG_DATARATE_100_HZ)) {
        std::cout << "Error setting magnetometer data rate on external bus";
        return false;
    }

    // Set up Slave0 to proxy Mag readings
    setBank(3);
    // set up slave0 to proxy reads to mag
    if (write(B3_REG_I2C_SLV0_ADDR, 0x8C)) {
        return false;
    }

    if (write(B3_REG_I2C_SLV0_REG, 0x10)) {
        return false;
    }

    if (write(B3_REG_I2C_SLV0_CTRL, 0x89)) {
        return false;
    }

    return true;
}

/*!
 *  @brief  Reads the specified Mag register
 *  @param  mag_reg_addr
 *          Address to be read
 *  @return Content of the specified Register
 */
unsigned int ICM20948::readMagRegister(unsigned int mag_reg_addr) {
    return readExternalRegister(0x8C, mag_reg_addr);
}

/*!
 *  @brief  Writes to the specified Mag register
 *  @param  mag_reg_addr
 *          Address to write to
 *  @param  value
 *          Value to write
 *  @return true: successful  false: failed
 */
bool ICM20948::writeMagRegister(unsigned int mag_reg_addr, unsigned int value) {
    return writeExternalRegister(0x0C, mag_reg_addr, value);
}

/*!
 *  @brief  Scales values directly read from registers to useful information
            and writes those to last read values
 */
void ICM20948::scaleValues(void) {

    gyro_range_t gyro_range = (gyro_range_t)current_gyro_range;
    accel_range_t accel_range = (accel_range_t)current_accel_range;

    float accel_scale = 1.0;
    float gyro_scale = 1.0;

    if (gyro_range == GYRO_RANGE_250_DPS)
        gyro_scale = 131.0;
    if (gyro_range == GYRO_RANGE_500_DPS)
        gyro_scale = 65.5;
    if (gyro_range == GYRO_RANGE_1000_DPS)
        gyro_scale = 32.8;
    if (gyro_range == GYRO_RANGE_2000_DPS)
        gyro_scale = 16.4;

    if (accel_range == ACCEL_RANGE_2_G)
        accel_scale = 16384.0;
    if (accel_range == ACCEL_RANGE_4_G)
        accel_scale = 8192.0;
    if (accel_range == ACCEL_RANGE_8_G)
        accel_scale = 4096.0;
    if (accel_range == ACCEL_RANGE_16_G)
        accel_scale = 2048.0;

    gyroX = rawGyroX / gyro_scale;
    gyroY = rawGyroY / gyro_scale;
    gyroZ = rawGyroZ / gyro_scale;

    accX = rawAccX / accel_scale;
    accY = rawAccY / accel_scale;
    accZ = rawAccZ / accel_scale;

    magX = rawMagX * UT_PER_LSB;
    magY = rawMagY * UT_PER_LSB;
    magZ = rawMagZ * UT_PER_LSB;
}

/*!
 *  @brief  Get the accelerometer's measurement range
 *  @return The accelerometer's measurement range
 */
accel_range_t ICM20948::getAccelRange(void) {
    return (accel_range_t)readAccelRange();
}

/*!
 *  @brief  Sets the accelerometer's measurement range
 *  @param  new_accel_range
 *          Measurement range to be set
 */
void ICM20948::setAccelRange(accel_range_t new_accel_range) {
    writeAccelRange((unsigned int)new_accel_range);
}

/*!
 *  @brief Get the gyro's measurement range
 *  @returns The gyro's measurement range
 */
gyro_range_t ICM20948::getGyroRange(void) {
    return (gyro_range_t)readGyroRange();
}

/*!
 *  @brief  Sets the gyro's measurement range
 *  @param  new_gyro_range
 *          Measurement range to be set
 */
void ICM20948::setGyroRange(gyro_range_t new_gyro_range) {
    writeGyroRange((unsigned int)new_gyro_range);
}

/*!
 *  @brief  Get the current magnetometer measurement rate
 *  @return the current rate
 */
ak09916_data_rate_t ICM20948::getMagDataRate(void) {

    uint8_t raw_mag_rate = readMagRegister(AK09916_CNTL2);
    return (ak09916_data_rate_t)(raw_mag_rate);
}

/*!
 *  @brief  Set the magnetometer measurement rate
 *  @param  rate
 *          The rate to set
 *  @return true: success false: failure
 */
bool ICM20948::setMagDataRate(ak09916_data_rate_t rate) {
  /*
   * Following the datasheet, the sensor will be set to
   * AK09916_MAG_DATARATE_SHUTDOWN followed by a 100ms delay, followed by
   * setting the new data rate.
   *
   * See page 9 of https://www.y-ic.es/datasheet/78/SMDSW.020-2OZ.pdf
   */

    // don't need to read/mask because there's nothing else in the register
    bool success = writeMagRegister(AK09916_CNTL2, AK09916_MAG_DATARATE_SHUTDOWN);
    Util::sleep(1);
    return writeMagRegister(AK09916_CNTL2, rate) && success;
}

ICM20948_Accelerometer::ICM20948_Accelerometer(ICM20948 *theICM20948) {
    _theICM20948 = theICM20948;
}

/*!
 *  @brief  Gets the sensor_t data for the ICM20948's accelerometer
 */
void ICM20948_Accelerometer::getSensor(sensor_t *sensor) {
    /* Clear the sensor_t object */
    memset(sensor, 0, sizeof(sensor_t));

    /* Insert the sensor name in the fixed length char array */
    strncpy(sensor->name, "ICM20948_A", sizeof(sensor->name) - 1);
    sensor->name[sizeof(sensor->name) - 1] = 0;
    sensor->version = 1;
    sensor->sensor_id = _sensorID;
    sensor->type = SENSOR_TYPE_ACCELEROMETER;
    sensor->min_delay = 0;
    sensor->min_value = -294.1995F; // -30g = -294.1995 m/s²
    sensor->max_value = 294.1995F;  //  30g =  294.1995 m/s²
    sensor->resolution = 0.122;     // 8192LSB/1000 mG -> 8.192 LSB/mG => 0.122 mG/LSB at +-4g
}

/*!
 *  @brief  Gets the accelerometer as a standard sensor event
 *  @param  t
 *          time in ms since program start
 *  @param  event
 *          Sensor event object that will be populated
 */
void ICM20948_Accelerometer::getEvent(unsigned int t, sensors_event_t *event) {
    _theICM20948->_read();
    _theICM20948->fillAccelEvent(event, t);
}

ICM20948_Gyro::ICM20948_Gyro(ICM20948 *theICM20948) {
    _theICM20948 = theICM20948;
}

/*!
 *  @brief  Gets the sensor_t data for the ICM20948's gyroscope sensor
 */
void ICM20948_Gyro::getSensor(sensor_t *sensor) {
    /* Clear the sensor_t object */
    memset(sensor, 0, sizeof(sensor_t));

    /* Insert the sensor name in the fixed length char array */
    strncpy(sensor->name, "ICM20948_G", sizeof(sensor->name) - 1);
    sensor->name[sizeof(sensor->name) - 1] = 0;
    sensor->version = 1;
    sensor->sensor_id = _sensorID;
    sensor->type = SENSOR_TYPE_GYROSCOPE;
    sensor->min_delay = 0;
    sensor->min_value = -69.81; /* -4000 dps -> rad/s (radians per second) */
    sensor->max_value = +69.81;
    sensor->resolution = 2.665e-7; /* 65.5 LSB/DPS */
}

/*!
 *  @brief  Gets the gyroscope as a standard sensor event
 *  @param  t
 *          time in ms since program start
 *  @param  event
 *          Sensor event object that will be populated
 */
void ICM20948_Gyro::getEvent(unsigned int t, sensors_event_t *event) {
    _theICM20948->_read();
    _theICM20948->fillGyroEvent(event, t);
}

ICM20948_Magnetometer::ICM20948_Magnetometer(ICM20948 *theICM20948) {
    _theICM20948 = theICM20948;
}

/*!
 *  @brief  Gets the sensor_t data for the ICM20948's magnetometer sensor
 */
void ICM20948_Magnetometer::getSensor(sensor_t *sensor) {
    /* Clear the sensor_t object */
    memset(sensor, 0, sizeof(sensor_t));

    /* Insert the sensor name in the fixed length char array */
    strncpy(sensor->name, "ICM20948_M", sizeof(sensor->name) - 1);
    sensor->name[sizeof(sensor->name) - 1] = 0;
    sensor->version = 1;
    sensor->sensor_id = _sensorID;
    sensor->type = SENSOR_TYPE_MAGNETIC_FIELD;
    sensor->min_delay = 0;
    sensor->min_value = -4900;
    sensor->max_value = 4900;
    sensor->resolution = 0.6667;
}

/*!
 *  @brief  Gets the magnetometer as a standard sensor event
 *  @param  t
 *          time in ms since program start
 *  @param  event
 *          Sensor event object that will be populated
 */
void ICM20948_Magnetometer::getEvent(unsigned int t, sensors_event_t *event) {
    _theICM20948->_read();
    _theICM20948->fillMagEvent(event, t);
}

} // namespace ICM20948
