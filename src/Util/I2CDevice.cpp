#include <pigpio.h>
#include <iostream>

#include "I2CDevice.h"

/*!
 *  @brief  Create an I2C device
 */
I2CDevice::I2CDevice() {}

/*!
 *  @brief  Create an I2C device at a given address
 *  @param  addr
 *          The 7-bit I2C address for the device
 *  @param  bus
 *          The I2C bus to use
 */
I2CDevice::I2CDevice(unsigned int addr, unsigned int bus) {
    _addr = addr;
    _bus = bus;
}

/*!
 *  @brief  Initializes and does basic address detection
 *  @param  initGpio
 *          True if gpioInitialise should be called
 *  @return 0 if I2C initialized else error code
 */
int I2CDevice::begin(bool initGpio) {
    // Initialise pigpio, returns the pigpio version number if OK, otherwise PI_INIT_FAILED(-1)
    if (initGpio && gpioInitialise() < 0) { return PI_INIT_FAILED; }

    // Get handle for the device at the given address on the given I2C bus
    int response = i2cOpen(_bus, _addr, 0);

    // Check if the handle is a error code aka < 0, if so return the error code
    if (response < 0) { return response; }

    // Set the handler for the instance
    _i2c = response;

    return 0;
}

/*!
 *  @brief  End the I2C connection
 *  @param  terminateGpio
 *          True if gpioTerminate should be called
 *  @return 0 if I2C handle closed else error code
 */
int I2CDevice::end(bool terminateGpio) {
    int response = i2cClose(_i2c);

    if (terminateGpio) { gpioTerminate(); } 

    return response;
}

/*!
 *  @brief  writes a byte of data to the specified address
 *  @param  addr
 *          the register to write to
 *  @param  message
 *          the byte to write
 *  @return 0 if success else error code
 */
int I2CDevice::write(unsigned int addr, unsigned int message) {
    return i2cWriteByteData(_i2c, addr, message);
}

/*!
 *  @brief  writes multiple bytes of data starting at the specified address
 *  @param  addr
 *          the first register to write to
 *  @param  message
 *          the bytes to write
 *  @param  bytes
 *          number of bytes to write
 *  @return 0 if success else error code
 */
int I2CDevice::writeBlock(unsigned int addr, char* message, unsigned int bytes) {
    return i2cWriteI2CBlockData(_i2c, addr, message, bytes);
}

/*!
 *  @brief  Writes bits to the specified address
 *          To write 0x02 to second- and third-left bit (0x40) of register 0x5B:
 *          addr:    0x5B
 *          message: 0x02
 *          bits:    2
 *          shift:   5
 *  @param  addr
 *          The register to write to
 *  @param  message
 *          The bits to write
 *  @param  bits
 *          The numer of bits to write (1..7)
 *  @param  shift
 *          The number of bits to left-shift the message in this register (0..7)
 *  @return 0 if success else error code
 */
int I2CDevice::writeBits(unsigned int addr, unsigned int message, unsigned int bits, unsigned int shift) {
    unsigned int val = read(addr);

    // mask off the data before writing
    unsigned int mask = (1 << (bits)) - 1;
    message &= mask;

    mask <<= shift;
    val &= ~mask;           // remove the current data at that spot
    val |= message << shift;// and add in the new data

    return write(addr, val);
}

/*!
 *  @brief  writes a 16 bit word to the specified address
 *  @param  addr
 *          the register to write to
 *  @param  message
 *          the bytes to write
 *  @return 0 if success else error code
 */
int I2CDevice::writeWord(unsigned int addr, unsigned int message) {
    return i2cWriteWordData(_i2c, addr, message);
}

/*!
 *  @brief  read a byte of data at the specified address
 *  @param  reg_addr
 *          the address to read
 *  @return the read data byte
 */
int I2CDevice::read(unsigned int addr) {
    return i2cReadByteData(_i2c, addr);
}

/*!
 *  @brief  reads multiple bytes of data starting at the specified address
 *  @param  addr
 *          the first register to read from
 *  @param  buffer
 *          the buffer to write the result to
 *  @param  bytes
 *          number of bytes to read
 *  @return 0 if success else error code
 */
int I2CDevice::readBlock(unsigned int addr, char *buffer, unsigned int bytes) {
    return i2cReadI2CBlockData(_i2c, addr, buffer, bytes);
}

/*!
 *  @brief  Reads bits from the specified address
 *          To read the second- and third-left bit from register 0x5B:
 *          addr:  0x5B
 *          bits:  2
 *          shift: 5
 *  @param  addr
 *          The register to write to
 *  @param  bits
 *          The numer of bits to write (1..7)
 *  @param  shift
 *          The number of bits to left-shift the message in this register (0..7)
 *  @return 0 if success else error code
 */
int I2CDevice::readBits(unsigned int addr, unsigned int bits, unsigned int shift) {
    unsigned int val = read(addr);
    val >>= shift;
    return val & ((1 << (bits)) - 1);
}

/*!
 *  @brief  Returns the 7-bit address of this device
 *  @return The 7-bit address of this device
 */
unsigned int I2CDevice::deviceAddress(void) { return _addr; }
