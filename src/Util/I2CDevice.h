#ifndef I2CDevice_h
#define I2CDevice_h

#include <stdint.h>

/*!
 *  @brief  Class that defines how we will talk to this device over I2C
 */
class I2CDevice {
public:
    I2CDevice();
    I2CDevice(unsigned int addr, unsigned int bus = 1);

    int begin(bool initGpio = true);
    int end(bool terminateGpio = true);

    int write(unsigned int addr, unsigned int message);
    int writeBlock(unsigned int addr, char* message, unsigned int bytes);
    int writeBits(unsigned int addr, unsigned int message, unsigned int bits, unsigned int shift);
    int writeWord(unsigned int addr, unsigned int message);
    int read(unsigned int addr);
    int readBlock(unsigned int addr, char *buffer, unsigned int bytes = 1);
    int readBits(unsigned int addr, unsigned int bits, unsigned int shift);

    unsigned int deviceAddress(void);
    
protected:
    unsigned int _i2c, _bus, _addr;
};

#endif // I2CDevice_h
