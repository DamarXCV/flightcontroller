#pragma once

#include <unistd.h>

namespace Util {
inline void sleep(unsigned int duration) {
    usleep(duration * 1000);
}

} // namespace Util 
