#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include <atomic>
#include <uWebSockets/App.h>

namespace Network {

struct WebsocketData {
    float roll {0};
    float pitch {0};
    float yaw {0};
    float inclination {0};
};

struct GlobalTelemetryData {
    std::atomic<float> roll {0};
    std::atomic<float> pitch {0};
    std::atomic<float> yaw {0};
    std::atomic<float> inclination {0};
};

extern GlobalTelemetryData gTelemetry;
extern GlobalTelemetryData gControlls;

extern uWS::App *gApp;


int startWebsocket();


} // namespace Network
#endif // WEBSOCKET_H
