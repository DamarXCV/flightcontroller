#include <uWebSockets/App.h>
#include <time.h>
#include <iostream>
#include <bitset>
#include <math.h>

#include "Websocket.h"

namespace Network {

GlobalTelemetryData gTelemetry;
GlobalTelemetryData gControlls;

uWS::App *gApp = nullptr;


int startWebsocket() {
    uWS::TemplatedApp<false>::WebSocketBehavior<WebsocketData> wsb = {
        /* Settings */
        .compression = uWS::SHARED_COMPRESSOR,
        .maxPayloadLength = 16 * 1024 * 1024,
        .idleTimeout = 16,
        .maxBackpressure = 1 * 1024 * 1024,
        .closeOnBackpressureLimit = false,
        .resetIdleTimeoutOnSend = false,
        .sendPingsAutomatically = true,
        /* Handlers */
        .upgrade = nullptr,
        .open = [](auto *ws) {
            /* Open event here, you may access ws->getUserData() which points to a WebsocketData struct */
            ws->subscribe("broadcast");    
            std::cout << "open called" << std::endl;
        },
        .message = [](auto *ws, std::string_view message, uWS::OpCode opCode) {
            WebsocketData data;
            memcpy(&data, message.data(), sizeof(data));
            
            gControlls.roll.store(data.roll, std::memory_order_release);
            gControlls.pitch.store(data.pitch, std::memory_order_release);
            gControlls.yaw.store(data.yaw, std::memory_order_release);
            gControlls.inclination.store(data.inclination, std::memory_order_release);
            
            std::cout << "Data - ";
            std::cout << "roll: " << gControlls.roll.load(std::memory_order_acquire);
            std::cout << ", pitch: " << gControlls.pitch.load(std::memory_order_acquire);
            std::cout << ", yaw: " << gControlls.yaw.load(std::memory_order_acquire);
            std::cout << ", inclination: " << gControlls.inclination.load(std::memory_order_acquire);
            std::cout << std::endl;
            
            switch (opCode)
            {
            case uWS::OpCode::TEXT:
                std::cout << "message called: OpCode.TEXT" << std::endl;
                break;
            case uWS::OpCode::BINARY:
                std::cout << "message called: OpCode.BINARY" << std::endl;
                break;
             default:
                std::cout << "message called: default" << std::endl;
                break;
            }
        },
        .drain = [](auto */*ws*/) {
            std::cout << "drain called" << std::endl;
            /* Check ws->getBufferedAmount() here */
        },
        .ping = [](auto */*ws*/, std::string_view) {
            std::cout << "ping called" << std::endl;
            /* Not implemented yet */
        },
        .pong = [](auto */*ws*/, std::string_view) {
            std::cout << "pong called" << std::endl;
            /* Not implemented yet */
        },
        .close = [](auto */*ws*/, int /*code*/, std::string_view /*message*/) {
            std::cout << "close called" << std::endl;
            /* You may access ws->getUserData() here */
        }
    };

    /* Keep in mind that uWS::App({options}) is the same as uWS::App() when compiled without SSL support.
     * You may swap to using uWS:App() if you don't need SSL */
    uWS::App app = uWS::App().ws<WebsocketData>("/*", std::move(wsb))
        .listen(9001, [](auto *listen_socket) {
            if (listen_socket) {
                std::cout << "Listening on port " << 9001 << std::endl;
            }
        });

    struct us_loop_t *loop = (struct us_loop_t *) uWS::Loop::get();
    struct us_timer_t *delayTimer = us_create_timer(loop, 0, 0);
    us_timer_set(delayTimer, [](struct us_timer_t */*t*/) {
        WebsocketData data;
        data.roll = gTelemetry.roll.load(std::memory_order_acquire);
        data.pitch = gTelemetry.pitch.load(std::memory_order_acquire);
        data.yaw = gTelemetry.yaw.load(std::memory_order_acquire);
        data.inclination = gTelemetry.inclination.load(std::memory_order_acquire);

        gApp->publish("broadcast", std::string_view((char*) &data, sizeof(data)), uWS::OpCode::BINARY, false);
    }, 100, 100);

    gApp = &app;
    app.run();

    return 0;
}

} // namespace Network
