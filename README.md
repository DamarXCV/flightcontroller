# Install
## Install required libraries
### pigpio
1. Clone the pigpio library

```
git clone --recurse-submodules https://github.com/joan2937/pigpio.git
```
2. Execute the Makefile with install parameter inside the repository
```
cd pigpio
sudo make
sudo make install
```

### uWebSockets
1. Clone the uWebSockets library

```
git clone --recurse-submodules https://github.com/uNetworking/uWebSockets.git
```
2. Execute the Makefile with install parameter inside the repository
```
cd uWebSockets
sudo make
sudo make install
```
4. Copy files and rename
```
sudo cp uSockets/uSockets.a /usr/local/lib/libusockets.a
sudo cp uSockets/libusockets.h /usr/local/include/libusockets.h
```
